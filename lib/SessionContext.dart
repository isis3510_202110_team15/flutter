import 'package:flutter/material.dart';
import 'package:reservapp_flutter/src/providers/UserProvider.dart';

class SessionContext extends InheritedWidget {
  static SessionContext _instancia;

  factory SessionContext({Key key, Widget child}) {
    if (_instancia == null) {
      _instancia = new SessionContext._internal(key: key, child: child);
    }

    return _instancia;
  }

  SessionContext._internal({Key key, Widget child})
      : super(key: key, child: child);

  final userProvider = UserProvider();

  @override
  bool updateShouldNotify(InheritedWidget oldWidget) => true;

  static UserProvider of(BuildContext context) {
    return context
        .dependOnInheritedWidgetOfExactType<SessionContext>()
        .userProvider;
  }
}
