import 'package:firebase_auth/firebase_auth.dart';
import 'package:flutter/material.dart';
import 'package:reservapp_flutter/routes.dart';
import 'package:reservapp_flutter/src/providers/AnalyticsProvider.dart';
import 'package:reservapp_flutter/src/providers/UserActionStadisticsProvider.dart';

class App extends StatelessWidget {
  @override
  Widget build(BuildContext context) {
    ROUTES initialRoute;
    try {
      FirebaseAuth.instance.currentUser.email;
      initialRoute = ROUTES.login;
    } catch (error) {
      initialRoute = ROUTES.login;
    }

    UserActionStadisticsProvider.calculateUserActionStatistics(context);

    return MaterialApp(
      navigatorObservers: [
        new AnalyticsProvider().getAnalyticsObserver(),
      ],
      debugShowCheckedModeBanner: false,
      title: 'ReservApp',
      initialRoute: initialRoute.toString(),
      routes: routes,
      theme: ThemeData(
        primaryColor: Colors.red,
      ),
    );
  }
}
