import 'package:flutter/material.dart';
import 'package:reservapp_flutter/src/model/Restaurant.dart';
import 'package:reservapp_flutter/src/offlineProviders/SharedPreferencesProvider.dart';
import 'package:reservapp_flutter/src/providers/RestaurantsProvider.dart';
import 'package:reservapp_flutter/src/providers/RestaurantsRecycler.dart';
import 'package:reservapp_flutter/src/utils/LocationsService.dart';
import 'package:reservapp_flutter/src/widgets/banners/NoConnectivityBanner.dart';
import 'package:reservapp_flutter/src/widgets/restaurantWidgets/RestaurantList.dart';
import '../widgets/SearchBar.dart';

class RestaurantsView extends StatefulWidget {
  final String category;
  RestaurantsView({this.category = ''});

  @override
  _RestaurantsViewState createState() =>
      _RestaurantsViewState(category: category);
}

class _RestaurantsViewState extends State<RestaurantsView> {
  RestaurantsRecycler _restaurantsRecycler;
  List<Restaurant> _restaurants;
  List<Restaurant> _filteredRestaurants = [];
  bool _locationDennied;
  final String category;
  bool _isLoading = true;

  _RestaurantsViewState({this.category = ''});

  void fetchRestaurants() async {
    setState(() {
      _isLoading = true;
    });

    RestaurantsProvider.getRestaurantsRecycler(context, category).then(
      (RestaurantsRecycler restaurantsRecycler) {
        if (restaurantsRecycler != null) {
          final restaurants = restaurantsRecycler.fetchNextRestaurants();
          setState(() {
            _restaurantsRecycler = restaurantsRecycler;
            _restaurants = _filteredRestaurants = restaurants;
            _isLoading = false;
          });
          fetchLocationDennied();
        } else {
          setState(() {
            _isLoading = false;
          });
        }
      },
    );
  }

  void fetchLocationDennied() async {
    await SharedPreferencesProvider.getBool(
      locationDenniedKey,
    ).then((locationDennied) {
      setState(() {
        _locationDennied = locationDennied;
      });
    });
  }

  @override
  void initState() {
    fetchRestaurants();
    super.initState();
  }

  void _filterRestaurants(searchText) {
    setState(
      () {
        _filteredRestaurants = RestaurantsProvider.filterRestaurants(
          context,
          _restaurants,
          searchText,
        );
      },
    );
  }

  @override
  Widget build(BuildContext context) {
    if (!_isLoading && _restaurantsRecycler == null) {
      return NoConnectivityBanner();
    }
    return _isLoading
        ? Center(
            child: CircularProgressIndicator(),
          )
        : Column(
            children: <Widget>[
              _locationDennied == true
                  ? MaterialButton(
                      child: Text(
                        'See restaurants near you',
                        style: TextStyle(
                          fontSize: 20.0,
                          color: Colors.white,
                          decoration: TextDecoration.none,
                        ),
                      ),
                      onPressed: () async {
                        await SharedPreferencesProvider.setBool(
                          locationDenniedKey,
                          false,
                        ).then((value) => fetchRestaurants());
                      },
                      color: Colors.blue[700],
                    )
                  : Container(),
              SearchBar(
                filterCategories: _filterRestaurants,
                placeholder: '(Name, address, category, city...)',
              ),
              Expanded(
                child: RestaurantList(_filteredRestaurants),
              ),
              _restaurantsRecycler != null && !_restaurantsRecycler.done
                  ? MaterialButton(
                      child: Text(
                        'Load more',
                        style: TextStyle(
                          fontSize: 20.0,
                          color: Colors.white,
                          decoration: TextDecoration.none,
                        ),
                      ),
                      onPressed: () {
                        final restaurants =
                            _restaurantsRecycler.fetchNextRestaurants();
                        setState(() {
                          _restaurants = _filteredRestaurants = restaurants;
                        });
                      },
                      color: Colors.blue[700],
                    )
                  : Container(),
              SizedBox(
                height: 10.0,
              )
            ],
          );
  }
}
