import 'package:flutter/material.dart';
import 'package:reservapp_flutter/src/model/Booking.dart';
import 'package:reservapp_flutter/src/providers/BookingsProvider.dart';
import 'package:reservapp_flutter/src/widgets/banners/NoConnectivityBanner.dart';
import 'package:reservapp_flutter/src/widgets/bookingWidgets/BookingsList.dart';

class BookingsView extends StatefulWidget {
  BookingsView();

  @override
  _BookingsViewState createState() => _BookingsViewState();
}

class _BookingsViewState extends State<BookingsView> {
  List<Booking> _bookings = [];
  bool _isLoading = true;

  _BookingsViewState();

  @override
  void initState() {
    BookingsProvider.getBookings(context).then((bookings) {
      setState(() {
        _bookings = bookings;
        _isLoading = false;
      });
    });
    super.initState();
  }

  @override
  Widget build(BuildContext context) {
    if (_bookings == null) {
      return NoConnectivityBanner();
    }

    return _isLoading
        ? Center(
            child: CircularProgressIndicator(),
          )
        : Container(
            child: BookingsList(_bookings),
          );
  }
}
