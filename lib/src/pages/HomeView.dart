import 'package:flutter/material.dart';
import 'package:reservapp_flutter/src/widgets/categoriesWidgets/CategoryList.dart';

class HomeView extends StatefulWidget {
  _HomeViewState createState() => _HomeViewState();
}

class _HomeViewState extends State<HomeView> {
  @override
  Widget build(BuildContext context) {
    return Container(
      margin: EdgeInsets.all(10.0),
      child: CategoryList(),
    );
  }
}
