import 'package:cached_network_image/cached_network_image.dart';
import 'package:cloud_firestore/cloud_firestore.dart';
import 'package:firebase_auth/firebase_auth.dart';
import 'package:flutter/cupertino.dart';
import 'package:flutter/material.dart';
import 'package:line_awesome_flutter/line_awesome_flutter.dart';
import 'package:reservapp_flutter/src/pages/AllergiesView.dart';
import '../widgets/containers/ViewContainer.dart';

class ProfileView extends StatefulWidget {
  @override
  _ProfileViewState createState() => new _ProfileViewState();
}

class _ProfileViewState extends State<ProfileView> {
  CollectionReference users = FirebaseFirestore.instance.collection('users');

  final _auth = FirebaseAuth.instance;

  Widget nombre(BuildContext context) {
    return FutureBuilder<DocumentSnapshot>(
      future: users.doc(_auth.currentUser.email).get(),
      builder:
          (BuildContext context, AsyncSnapshot<DocumentSnapshot> snapshot) {
        if (snapshot.hasError) {
          return Text("Something went wrong");
        }

        if (snapshot.connectionState == ConnectionState.done) {
          Map<String, dynamic> data = snapshot.data.data();
          return Text(
            "${data['fullName']}",
            style: TextStyle(color: Colors.white),
          );
        }

        return Text("loading");
      },
    );
  }

  Widget cedula(BuildContext context) {
    return FutureBuilder<DocumentSnapshot>(
      future: users.doc(_auth.currentUser.email).get(),
      builder:
          (BuildContext context, AsyncSnapshot<DocumentSnapshot> snapshot) {
        if (snapshot.hasError) {
          return Text("Something went wrong");
        }

        if (snapshot.connectionState == ConnectionState.done) {
          Map<String, dynamic> data = snapshot.data.data();
          return Text("${data['id']} ", style: TextStyle(color: Colors.white));
        }

        return Text("loading");
      },
    );
  }

  Widget telefono(BuildContext context) {
    return FutureBuilder<DocumentSnapshot>(
      future: users.doc(_auth.currentUser.email).get(),
      builder:
          (BuildContext context, AsyncSnapshot<DocumentSnapshot> snapshot) {
        if (snapshot.hasError) {
          return Text("Something went wrong");
        }

        if (snapshot.connectionState == ConnectionState.done) {
          Map<String, dynamic> data = snapshot.data.data();
          return Text("${data['phone']} ",
              style: TextStyle(color: Colors.white));
        }

        return Text("loading");
      },
    );
  }

  Widget _crearAlergia(BuildContext context) {
    return StreamBuilder(
      builder: (BuildContext context, AsyncSnapshot snapshot) {
        return Container(
          padding: EdgeInsets.symmetric(horizontal: 20.0),
          child: TextField(
            keyboardType: TextInputType.text,
            decoration: InputDecoration(
              icon: Icon(Icons.alternate_email, color: Colors.red),
              hintText: 'example@email.com',
              labelText: 'Email',
            ),
          ),
        );
      },
    );
  }

  @override
  Widget build(BuildContext context) {
    var color1 = Colors.black38;
    var color2 = Colors.black;
    return new Scaffold(
      appBar: new AppBar(
        title: new Text('My Profile'),
      ),
      body: Column(
        children: <Widget>[
          ClipRRect(
            borderRadius: BorderRadius.circular(10000.0),
            child: CachedNetworkImage(
              placeholder: (context, url) => Container(
                margin: EdgeInsets.all(20.0),
                child: CircularProgressIndicator(),
              ),
              imageUrl: 'https://i.pravatar.cc/150?img=2',
            ),
          ),
          Align(
            alignment: Alignment.bottomCenter,
          ),
          SizedBox(height: 10.0 * 2),
          Container(
            height: 10.0 * 4,
            width: 10.0 * 30,
            decoration: BoxDecoration(
              borderRadius: BorderRadius.circular(10.0 * 3),
              color: color1,
            ),
            child: Row(
              children: [
                SizedBox(width: 5),
                Icon(
                  LineAwesomeIcons.user,
                  size: 10.5 * 3,
                ),
                SizedBox(
                  width: 10.0 * 4,
                ),
                nombre(context)
              ],
            ),
          ),
          new Divider(
            color: color2,
          ),
          Container(
            height: 10.0 * 4,
            width: 10.0 * 30,
            decoration: BoxDecoration(
              borderRadius: BorderRadius.circular(10.0 * 3),
              color: color1,
            ),
            child: Row(
              children: [
                SizedBox(width: 5),
                Icon(
                  LineAwesomeIcons.identification_badge,
                  size: 10.5 * 3,
                ),
                SizedBox(
                  width: 10.0 * 4,
                ),
                cedula(context)
              ],
            ),
          ),
          new Divider(
            color: color2,
          ),
          Container(
            height: 10.0 * 4,
            width: 10.0 * 30,
            decoration: BoxDecoration(
              borderRadius: BorderRadius.circular(10.0 * 3),
              color: color1,
            ),
            child: Row(
              children: [
                SizedBox(width: 5),
                Icon(
                  LineAwesomeIcons.phone,
                  size: 10.5 * 3,
                ),
                SizedBox(
                  width: 10.0 * 4,
                ),
                telefono(context)
              ],
            ),
          ),
          new Divider(
            color: color2,
          ),
          Container(
            height: 10.0 * 4,
            width: 10.0 * 30,
            decoration: BoxDecoration(
              borderRadius: BorderRadius.circular(10.0 * 3),
              color: color1,
            ),
            child: Row(
              children: [
                SizedBox(width: 5),
                Icon(
                  LineAwesomeIcons.allergies,
                  size: 10.5 * 3,
                ),
                SizedBox(
                  width: 10.0 * 4,
                ),
                Text('Allergies', style: TextStyle(color: Colors.white)),
                SizedBox(
                  width: 10.0 * 10,
                ),
                IconButton(
                  icon: Icon(Icons.add, color: Colors.white),
                  splashColor: CupertinoColors.white,
                  onPressed: () {
                    Navigator.of(context).pop();
                    Navigator.push(
                      context,
                      new MaterialPageRoute(
                        builder: (BuildContext context) => new ViewContainer(
                          title: "Allergies",
                          child: AllergiesView(),
                        ),
                      ),
                    );
                  },
                ),
              ],
            ),
          ),
        ],
      ),
    );
  }
}
