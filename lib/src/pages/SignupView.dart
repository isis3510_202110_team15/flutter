import 'package:cloud_firestore/cloud_firestore.dart';
import 'package:firebase_crashlytics/firebase_crashlytics.dart';
import 'package:flutter/material.dart';
import 'package:reservapp_flutter/routes.dart';
import 'package:reservapp_flutter/src/providers/UserProvider.dart';
import 'package:reservapp_flutter/SessionContext.dart';
import 'package:firebase_auth/firebase_auth.dart';
import 'package:reservapp_flutter/src/providers/AnalyticsProvider.dart';
import 'package:reservapp_flutter/src/utils/checkConnectivity.dart';

class SignupView extends StatelessWidget {
  String tipoDoc = 'C.C';
  String id = '';
  String name = '';
  String phone = '';

  @override
  Widget build(BuildContext context) {
    new AnalyticsProvider().getAnalytics().setCurrentScreen(
          screenName: ROUTES.signup.toString(),
        );
    return Scaffold(
      body: Stack(
        children: <Widget>[
          _crearFondo(context),
          _registerForm(context),
        ],
      ),
    );
  }

  Widget _registerForm(BuildContext context) {
    final _auth = FirebaseAuth.instance;
    final bloc = SessionContext.of(context);
    final size = MediaQuery.of(context).size;
    CollectionReference users = FirebaseFirestore.instance.collection('users');

    return SingleChildScrollView(
      child: Column(
        children: <Widget>[
          SafeArea(
            child: Container(
              height: 180.0,
            ),
          ),
          Container(
            width: size.width * 0.85,
            margin: EdgeInsets.symmetric(vertical: 30.0),
            padding: EdgeInsets.symmetric(vertical: 50.0),
            decoration: BoxDecoration(
              color: Colors.white,
              borderRadius: BorderRadius.circular(5.0),
              boxShadow: <BoxShadow>[
                BoxShadow(
                    color: Colors.black26,
                    blurRadius: 3.0,
                    offset: Offset(0.0, 5.0),
                    spreadRadius: 3.0)
              ],
            ),
            child: Column(
              children: <Widget>[
                Container(
                  margin: EdgeInsets.symmetric(horizontal: 30.0),
                  child: Text(
                      'Register and start to book in some of the restaurants of your city',
                      style: TextStyle(fontSize: 20.0)),
                ),
                SizedBox(height: 30.0),
                _crearTipoDoc(bloc),
                SizedBox(height: 10.0),
                _crearId(bloc),
                SizedBox(height: 10.0),
                _crearName(bloc),
                SizedBox(height: 10.0),
                _crearPhone(bloc),
                SizedBox(height: 10.0),
                _crearEmail(bloc),
                SizedBox(height: 10.0),
                _crearPassword(bloc),
                SizedBox(height: 10.0),
                _crearBoton(bloc, _auth, users)
              ],
            ),
          ),
          FlatButton(
            child: Text('Already have an account? Login'),
            onPressed: () => Navigator.pushReplacementNamed(
              context,
              ROUTES.login.toString(),
            ),
          ),
          SizedBox(height: 100.0),
        ],
      ),
    );
  }

  Widget _crearEmail(UserProvider bloc) {
    return StreamBuilder(
      stream: bloc.emailStream,
      builder: (BuildContext context, AsyncSnapshot snapshot) {
        return Container(
          padding: EdgeInsets.symmetric(horizontal: 20.0),
          child: TextField(
            keyboardType: TextInputType.emailAddress,
            decoration: InputDecoration(
                icon: Icon(Icons.alternate_email, color: Colors.red),
                hintText: 'example@email.com',
                labelText: 'Email',
                counterText: snapshot.data,
                errorText: snapshot.error),
            onChanged: bloc.changeEmail,
          ),
        );
      },
    );
  }

  Widget _crearPassword(UserProvider bloc) {
    return StreamBuilder(
      stream: bloc.passwordStream,
      builder: (BuildContext context, AsyncSnapshot snapshot) {
        return Container(
          padding: EdgeInsets.symmetric(horizontal: 20.0),
          child: TextField(
            obscureText: true,
            decoration: InputDecoration(
                icon: Icon(Icons.lock_outline, color: Colors.red),
                labelText: 'Password',
                errorText: snapshot.error),
            onChanged: bloc.changePassword,
          ),
        );
      },
    );
  }

  Widget _crearBoton(
      UserProvider bloc, FirebaseAuth _auth, CollectionReference users) {
    User currentUser;

    return StreamBuilder(
      stream: bloc.formValidStream,
      builder: (BuildContext context, AsyncSnapshot snapshot) {
        return RaisedButton(
          child: Container(
            padding: EdgeInsets.symmetric(
              horizontal: 80.0,
              vertical: 15.0,
            ),
            child: Text('Register'),
          ),
          shape: RoundedRectangleBorder(
            borderRadius: BorderRadius.circular(5.0),
          ),
          elevation: 0.0,
          color: Colors.red,
          textColor: Colors.white,
          onPressed: snapshot.hasData
              ? () async {
                  if (await checkConnectivity(context)) {
                    try {
                      final newUser =
                          await _auth.createUserWithEmailAndPassword(
                        email: bloc.email,
                        password: bloc.password,
                      );
                      await addUser(users, bloc);
                      if (newUser != null) {
                        Navigator.pushNamed(
                          context,
                          ROUTES.login.toString(),
                        );
                        currentUser = _auth.currentUser;
                      }
                    } on Exception catch (e, s) {
                      await FirebaseCrashlytics.instance.recordError(e, s);
                    }
                  }
                }
              : null,
        );
      },
    );
  }

  Widget _crearFondo(BuildContext context) {
    final size = MediaQuery.of(context).size;

    final fondoModaro = Container(
      height: size.height * 0.4,
      width: double.infinity,
      decoration: BoxDecoration(
        gradient: LinearGradient(
          colors: <Color>[
            Color.fromRGBO(211, 47, 47, 1.0),
            Color.fromRGBO(195, 45, 45, 1.0)
          ],
        ),
      ),
    );

    final circulo = Container(
      width: 100.0,
      height: 100.0,
      decoration: BoxDecoration(
        borderRadius: BorderRadius.circular(100.0),
        color: Color.fromRGBO(255, 255, 255, 0.05),
      ),
    );

    return Stack(
      children: <Widget>[
        fondoModaro,
        Positioned(top: 90.0, left: 30.0, child: circulo),
        Positioned(top: -40.0, right: -30.0, child: circulo),
        Positioned(bottom: -50.0, right: -10.0, child: circulo),
        Positioned(bottom: 120.0, right: 20.0, child: circulo),
        Positioned(bottom: -50.0, left: -20.0, child: circulo),
        Container(
          padding: EdgeInsets.only(top: 80.0),
          child: Column(
            children: <Widget>[
              Center(
                child: Image(
                    image: AssetImage('assets/contract.png'),
                    fit: BoxFit.cover,
                    height: 150.0,
                    width: 150.0),
              ),
            ],
          ),
        ),
      ],
    );
  }

  Widget _crearTipoDoc(UserProvider bloc) {
    return StreamBuilder(
      stream: bloc.tipoDocStream,
      builder: (BuildContext context, AsyncSnapshot snapshot) {
        if (snapshot.hasData == true) {
          tipoDoc = snapshot.data;
        } else {
          tipoDoc = 'C.C';
        }
        return Row(
          children: [
            Container(
              margin: EdgeInsets.symmetric(horizontal: 30.0),
              child: Text('ID Type',
                  style: TextStyle(color: Colors.black, fontSize: 15.0)),
            ),
            DropdownButton<String>(
              value: bloc.tipoDoc,
              items: [
                DropdownMenuItem(
                  child: Text('C.C'),
                  value: 'C.C',
                ),
                DropdownMenuItem(
                  child: Text('T.I'),
                  value: 'T.I',
                ),
                DropdownMenuItem(
                  child: Text('NIT'),
                  value: 'NIT',
                ),
                DropdownMenuItem(
                  child: Text('Pasaporte'),
                  value: 'Pasaporte',
                ),
              ],
              onChanged: bloc.changeTipoDoc,
            )
          ],
        );
      },
    );
  }

  Widget _crearId(UserProvider bloc) {
    return StreamBuilder(
      stream: bloc.idStream,
      builder: (BuildContext context, AsyncSnapshot snapshot) {
        if (snapshot.hasData == true) {
          id = snapshot.data;
        } else {
          id = '';
        }

        return Container(
          padding: EdgeInsets.symmetric(horizontal: 20.0),
          child: TextField(
            obscureText: false,
            decoration: InputDecoration(
              icon: Icon(
                Icons.perm_identity,
                color: Colors.red,
              ),
              labelText: 'ID',
              errorText: snapshot.error,
            ),
            onChanged: bloc.changeId,
          ),
        );
      },
    );
  }

  Widget _crearName(UserProvider bloc) {
    return StreamBuilder(
      stream: bloc.nameStream,
      builder: (BuildContext context, AsyncSnapshot snapshot) {
        if (snapshot.hasData == true) {
          name = snapshot.data;
        } else {
          name = '';
        }

        return Container(
          padding: EdgeInsets.symmetric(horizontal: 20.0),
          child: TextField(
            obscureText: false,
            decoration: InputDecoration(
              icon: Icon(Icons.person, color: Colors.red),
              labelText: 'Name',
              errorText: snapshot.error,
            ),
            onChanged: bloc.changeName,
          ),
        );
      },
    );
  }

  Widget _crearPhone(UserProvider bloc) {
    return StreamBuilder(
      stream: bloc.phoneStream,
      builder: (BuildContext context, AsyncSnapshot snapshot) {
        if (snapshot.hasData == true) {
          phone = snapshot.data;
        } else {
          phone = '';
        }

        return Container(
          padding: EdgeInsets.symmetric(horizontal: 20.0),
          child: TextField(
            obscureText: false,
            decoration: InputDecoration(
              icon: Icon(Icons.phone, color: Colors.red),
              labelText: 'Phone',
              errorText: snapshot.error,
            ),
            onChanged: bloc.changePhone,
          ),
        );
      },
    );
  }

  Future<void> addUser(CollectionReference users, UserProvider bloc) {
    return users
        .doc(bloc.email)
        .set({
          'fullName': name,
          'id': id,
          'idType': tipoDoc,
          'phone': phone,
          'allergies': []
        })
        .then((value) => {})
        .catchError((error) => {});
  }
}
