import 'package:flutter/cupertino.dart';
import 'package:flutter/material.dart';
import 'package:reservapp_flutter/src/providers/AllergiesProvider.dart';
import 'package:reservapp_flutter/src/utils/checkConnectivity.dart';
import 'package:reservapp_flutter/src/widgets/banners/NoConnectivityBanner.dart';

class AllergiesView extends StatefulWidget {
  @override
  _AllergiesViewState createState() => new _AllergiesViewState();
}

class _AllergiesViewState extends State<AllergiesView> {
  Map<String, bool> _allergies;
  bool _isLoading = true;
  bool _hasConnectivity = true;

  _AllergiesViewState() {
    ch();
  }

  void ch() async {
    final hasConnectivity = await checkConnectivity(context);
    setState(() {
      _hasConnectivity = hasConnectivity;
    });
  }

  void onChecked(
    String allergyName,
    bool allergyStatus,
  ) async {
    final newStatus = await AllergiesProvider.patchAllergy(
      context,
      allergyName,
      allergyStatus,
    );
    final newAllergies = _allergies;
    newAllergies[allergyName] = newStatus;

    final hasConnectivity = await checkConnectivity(context);

    setState(() {
      _allergies = newAllergies;
      _hasConnectivity = hasConnectivity;
    });
  }

  @override
  void initState() {
    AllergiesProvider.getAllergies(context).then(
      (allergies) {
        setState(() {
          _allergies = allergies;
          _isLoading = false;
        });
      },
    );
    super.initState();
  }

  @override
  Widget build(BuildContext context) {
    return _isLoading
        ? Center(
            child: CircularProgressIndicator(),
          )
        : ListView(
            children: [
              ..._allergies.entries
                  .map(
                    (allergy) => buildSingleCheckbox(
                      allergy.key,
                      allergy.value,
                    ),
                  )
                  .toList(),
              SizedBox(
                height: 20.0,
              ),
              !_isLoading && !_hasConnectivity
                  ? NoConnectivityBanner(
                      text:
                          'You can edit your allergies once you have connection!',
                    )
                  : Container()
            ],
          );
  }

  Widget buildSingleCheckbox(
    String allergyName,
    bool allergyStatus,
  ) =>
      ListTile(
        onTap: () => onChecked(
          allergyName,
          !allergyStatus,
        ),
        leading: Checkbox(
          value: allergyStatus,
          onChanged: (newStatus) => onChecked(
            allergyName,
            newStatus,
          ),
        ),
        title: Text(
          allergyName,
          style: TextStyle(
            fontSize: 20,
            fontWeight: FontWeight.bold,
          ),
        ),
      );
}
