import 'package:flutter/material.dart';
import 'package:reservapp_flutter/src/widgets/analyticsWidgets/AnalyticsList.dart';

class AnalyticsView extends StatefulWidget {
  _AnalyticsViewState createState() => _AnalyticsViewState();
}

class _AnalyticsViewState extends State<AnalyticsView> {
  @override
  Widget build(BuildContext context) {
    return Container(
      margin: EdgeInsets.all(10.0),
      child: AnalyticsList(),
    );
  }
}
