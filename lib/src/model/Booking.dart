import 'package:cloud_firestore/cloud_firestore.dart';
import 'package:reservapp_flutter/src/model/UserAction.dart';
import 'package:reservapp_flutter/src/offlineProviders/DatabaseProvider.dart';

final String bookingTableName = 'bookings';

final String imageKey = 'image';
final String restaurantAddressKey = 'restaurantAddress';
final String restaurantNameKey = 'restaurantName';
final String restaurantIdKey = 'restaurantId';
final String restaurantCategoryKey = 'restaurantCategory';
final String dateKey = 'date';
final String peopleQuantityKey = 'peopleQuantity';

class Booking {
  String id;
  DateTime date;
  int peopleQuantity;
  final String image;
  final String userId;
  final String restaurantAddress;
  final String restaurantName;
  final String restaurantId;
  final String restaurantCategory;

  Booking({
    this.id,
    this.date,
    this.peopleQuantity,
    this.image,
    this.userId,
    this.restaurantId,
    this.restaurantName,
    this.restaurantAddress,
    this.restaurantCategory,
  });

  factory Booking.fromFirestore(QueryDocumentSnapshot bookingDoc) {
    return Booking(
      id: bookingDoc.reference.id,
      date: bookingDoc[dateKey].toDate(),
      peopleQuantity: bookingDoc[peopleQuantityKey],
      image: bookingDoc[imageKey],
      userId: bookingDoc[userIdKey],
      restaurantId: bookingDoc[restaurantIdKey],
      restaurantName: bookingDoc[restaurantNameKey],
      restaurantAddress: bookingDoc[restaurantAddressKey],
      restaurantCategory: bookingDoc[restaurantCategoryKey],
    );
  }

  factory Booking.fromSQLRow(Map<String, Object> bookingRow) {
    return Booking(
      id: bookingRow[idKey],
      image: bookingRow[imageKey],
      restaurantAddress: bookingRow[restaurantAddressKey],
      restaurantName: bookingRow[restaurantNameKey],
      restaurantId: bookingRow[restaurantIdKey],
      restaurantCategory: bookingRow[restaurantCategoryKey],
      userId: bookingRow[userIdKey],
      date: DateTime.parse(bookingRow[dateKey]),
      peopleQuantity: bookingRow[peopleQuantityKey],
    );
  }

  bool isPast() {
    DateTime now = DateTime.now();
    return now.compareTo(date) > 0;
  }

  Map<String, dynamic> toFirebaseDoc() => {
        dateKey: date,
        peopleQuantityKey: peopleQuantity,
        imageKey: image,
        userIdKey: userId,
        restaurantIdKey: restaurantId,
        restaurantNameKey: restaurantName,
        restaurantAddressKey: restaurantAddress,
        restaurantCategoryKey: restaurantCategory,
      };

  Map<String, Object> toSQLRow() => {
        idKey: id,
        dateKey: date.toIso8601String(),
        peopleQuantityKey: peopleQuantity,
        imageKey: image,
        userIdKey: userId,
        restaurantIdKey: restaurantId,
        restaurantNameKey: restaurantName,
        restaurantAddressKey: restaurantAddress,
        restaurantCategoryKey: restaurantCategory,
      };

  static List<Booking> makeBookingListFromQuerySnapshot(
    QuerySnapshot bookingsQuerySnapshot,
  ) {
    List<Booking> bookings = [];

    for (var i = 0; i < bookingsQuerySnapshot.docs.length; i++) {
      bookings.add(Booking.fromFirestore(bookingsQuerySnapshot.docs[i]));
    }

    return bookings;
  }

  static List<Booking> makeBookingListFromSQLRows(
    List<Map<String, Object>> bookingRows,
  ) {
    List<Booking> bookings = [];

    for (var i = 0; i < bookingRows.length; i++) {
      bookings.add(Booking.fromSQLRow(bookingRows[i]));
    }

    return bookings;
  }
}
