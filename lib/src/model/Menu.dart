import 'package:reservapp_flutter/src/model/Dish.dart';

final String menuTableName = 'menu';

String drinksKey = 'drinks';
String breakfastsKey = 'entrees';
String entreesKey = 'entrees';
String mainsKey = 'mains';
String dessertsKey = 'desserts';

class Menu {
  final List<Dish> drinks;
  final List<Dish> breakfasts;
  final List<Dish> entrees;
  final List<Dish> mains;
  final List<Dish> desserts;

  Menu({
    this.drinks,
    this.breakfasts,
    this.entrees,
    this.mains,
    this.desserts,
  });

  String toString() =>
      "${drinks.toString()} ${breakfasts.toString()} ${entrees.toString()} ${mains.toString()} ${desserts.toString()}";
}
