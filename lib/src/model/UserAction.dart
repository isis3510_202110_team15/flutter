import 'package:cloud_firestore/cloud_firestore.dart';
import 'package:firebase_auth/firebase_auth.dart';

final String userIdKey = 'userId';
final String restaurantIdKey = 'restaurantId';
final String restaurantNameKey = 'restaurantName';
final String restaurantAddressKey = 'restaurantAddress';
final String restaurantCategoryKey = 'restaurantCategory';
final String dateKey = 'date';
final String hadConnectionKey = 'hadConnection';
final String typeKey = 'type';
final String viewKey = 'view';

enum USER_ACTION_TYPES {
  viewMenu,
  viewCategory,
  viewEditBookingForm,
  viewBookingForm,
  saveBooking,
  editBooking,
}

enum VIEWS {
  allergies,
  bookings,
  bookingsDialog,
  home,
  login,
  profile,
  restaurants,
  restaurantsDialog,
  signup,
  qrCode,
  notification,
}

class UserAction {
  String userId;
  DateTime date;
  final String restaurantId;
  final String restaurantName;
  final String restaurantAddress;
  final String restaurantCategory;
  final bool hadConnection;
  String _type;
  String _view;

  UserAction({
    this.userId,
    this.restaurantId,
    this.restaurantName,
    this.restaurantAddress,
    this.restaurantCategory,
    this.hadConnection: true,
    this.date,
    dynamic type,
    dynamic view,
  }) {
    userId = userId == null ? FirebaseAuth.instance.currentUser.email : userId;
    date = date == null ? DateTime.now() : date;
    _type = type.toString();
    _view = view.toString();
  }

  factory UserAction.fromFirestore(
    QueryDocumentSnapshot userActionDoc,
  ) {
    return UserAction(
      userId: userActionDoc[userIdKey],
      restaurantId: userActionDoc[restaurantIdKey],
      restaurantName: userActionDoc[restaurantNameKey],
      restaurantAddress: userActionDoc[restaurantAddressKey],
      restaurantCategory: userActionDoc[restaurantCategoryKey],
      hadConnection: userActionDoc[hadConnectionKey],
      date: userActionDoc[dateKey].toDate(),
      type: userActionDoc[typeKey],
      view: userActionDoc[viewKey],
    );
  }

  Map<String, dynamic> toFirebaseDoc() => {
        userIdKey: userId,
        restaurantIdKey: restaurantId,
        restaurantNameKey: restaurantName,
        restaurantAddressKey: restaurantAddress,
        restaurantCategoryKey: restaurantCategory,
        dateKey: date,
        hadConnectionKey: hadConnection,
        typeKey: _type,
        viewKey: _view,
      };

  Map<String, Object> toSQLRow() => {
        userIdKey: userId,
        restaurantIdKey: restaurantId,
        restaurantNameKey: restaurantName,
        restaurantAddressKey: restaurantAddress,
        restaurantCategoryKey: restaurantCategory,
        dateKey: date.toIso8601String(),
        hadConnectionKey: hadConnection,
        typeKey: _type,
        viewKey: _view,
      };

  static List<UserAction> makeUserActionListFromQuerySnapshot(
    QuerySnapshot userActionsQuerySnapshot,
  ) {
    List<UserAction> userActions = [];

    for (var i = 0; i < userActionsQuerySnapshot.docs.length; i++) {
      userActions
          .add(UserAction.fromFirestore(userActionsQuerySnapshot.docs[i]));
    }

    return userActions;
  }
}
