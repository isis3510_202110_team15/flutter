import 'package:cloud_firestore/cloud_firestore.dart';
import 'package:location/location.dart';
import 'package:reservapp_flutter/src/utils/LocationsService.dart';

final String restaurantsTableName = 'restaurants';

final String qrCodeKey = 'qrCode';
final String addressKey = 'address';
final String categoryKey = 'categoryName';
final String cityKey = 'city';
final String descriptionKey = 'description';
final String imageKey = 'image';
final String latitudeKey = 'latitude';
final String longitudeKey = 'longitude';
final String nameKey = 'name';
final String stateKey = 'state';
final String kmAwayKey = 'kmAway';
final String userActionCountKey = 'userActionCount';

class Restaurant {
  final String id;
  final String qrCode;
  final String address;
  final String category;
  final String city;
  final String description;
  final String image;
  final double latitude;
  final double longitude;
  final String name;
  final String state;
  final LocationData currentLocationData;
  double kmAway;
  int userActionCount;

  Restaurant({
    this.id,
    this.qrCode,
    this.address,
    this.category,
    this.city,
    this.description,
    this.image,
    this.latitude,
    this.longitude,
    this.name,
    this.state,
    this.currentLocationData,
    this.userActionCount,
    this.kmAway,
  }) {
    if (kmAway == null && currentLocationData != null) {
      double distanceInKm = LocationsService.getKmBetweenTwoCoordinates(
        currentLocationData.latitude,
        currentLocationData.longitude,
        latitude,
        longitude,
      );
      kmAway = distanceInKm;
    }
  }

  factory Restaurant.fromFirestore({
    QueryDocumentSnapshot restaurantDoc,
    LocationData currentLocationData,
    double kmAway,
    int userActionCount,
  }) {
    return Restaurant(
      id: restaurantDoc.reference.id,
      qrCode: restaurantDoc[qrCodeKey],
      name: restaurantDoc[nameKey],
      image: restaurantDoc[imageKey],
      description: restaurantDoc[descriptionKey],
      address: restaurantDoc[addressKey],
      city: restaurantDoc[cityKey],
      category: restaurantDoc[categoryKey],
      state: restaurantDoc[stateKey],
      latitude: restaurantDoc[latitudeKey],
      longitude: restaurantDoc[longitudeKey],
      currentLocationData: currentLocationData,
      kmAway: kmAway,
      userActionCount: userActionCount,
    );
  }

  String toString() => "$address $category $city $description $name $state";

  static Future<List<Restaurant>> makeRestaurantListFromQuerySnapshot(
    QuerySnapshot restaurantsQuerySnapshot,
  ) async {
    List<Restaurant> restaurants = [];

    LocationData currentLocationData =
        await LocationsService.getCurrentCoordinates();

    for (var i = 0; i < restaurantsQuerySnapshot.docs.length; i++) {
      restaurants.add(
        Restaurant.fromFirestore(
          restaurantDoc: restaurantsQuerySnapshot.docs[i],
          currentLocationData: currentLocationData,
        ),
      );
    }

    return restaurants;
  }

  bool isFavorite() {
    final bool isFavorite = userActionCount > 5;
    return isFavorite;
  }

  int compareTo(Restaurant restaurant) {
    if (kmAway != null &&
        restaurant.kmAway != null &&
        userActionCount != null &&
        restaurant.userActionCount != null) {
      if (userActionCount == restaurant.userActionCount) {
        return kmAway.compareTo(restaurant.kmAway);
      }
      return restaurant.userActionCount.compareTo(userActionCount);
    }
    return 0;
  }
}
