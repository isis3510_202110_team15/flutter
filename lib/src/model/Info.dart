class Info {
  String favCat;
  String favRest;
  String favTime;
  String favNeigh;
  String userCategory;

  Info({
    this.favCat,
    this.favNeigh,
    this.favRest,
    this.favTime,
    this.userCategory,
  });
}
