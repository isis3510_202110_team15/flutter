final String dishTableName = 'dishes';

final String nameKey = 'name';
final String descriptionKey = 'description';
final String priceKey = 'price';
final String restaurantIdKey = 'restaurantId';
final String ingredientsKey = 'ingredients';

class Dish {
  final String name;
  final String description;
  final double price;
  final List<dynamic> ingredients;

  Dish({
    this.name,
    this.description,
    this.price,
    this.ingredients,
  });

  factory Dish.fromFirestore(Map<String, dynamic> dishDoc) {
    return Dish(
      name: dishDoc[nameKey],
      description: dishDoc[descriptionKey],
      price: dishDoc[priceKey],
      ingredients: dishDoc[ingredientsKey],
    );
  }

  Map<String, Object> toJson(String restaurantId) => {
        nameKey: name,
        descriptionKey: description,
        priceKey: price,
        restaurantIdKey: restaurantId,
        ingredientsKey: ingredients,
      };

  String toString() => "$name $description $price ${ingredients.toString()}";

  static List<Dish> makeDishesFromDocs(
    List<dynamic> dishesDocs,
  ) {
    List<Dish> dishes = [];

    for (var i = 0; i < dishesDocs.length; i++) {
      dishes.add(Dish.fromFirestore(Map<String, dynamic>.from(dishesDocs[i])));
    }

    return dishes;
  }
}
