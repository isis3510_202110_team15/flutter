import 'package:cloud_firestore/cloud_firestore.dart';

final String categoryTableName = 'categories';

class Category {
  String id;
  String title;
  String img;
  Category({this.id, this.title, this.img});

  factory Category.fromFirestore(DocumentSnapshot doc) {
    Map data = doc.data();

    return Category(
      id: doc.id,
      title: data['name'] ?? null,
      img: data['image'] ?? null,
    );
  }
}
