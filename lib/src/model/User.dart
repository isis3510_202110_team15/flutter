final String usersTableName = 'users';

final String allergiesKey = 'allergies';

class User {
  int id;
  String name;
  String email;
  String cellPhone;
  String tipoDoc;
  List allergies;

  User({
    this.id,
    this.name,
    this.email,
    this.cellPhone,
    this.tipoDoc,
    this.allergies,
  });
}
