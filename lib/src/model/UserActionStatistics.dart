import 'package:cloud_firestore/cloud_firestore.dart';
import 'package:reservapp_flutter/src/model/UserAction.dart';
import 'package:reservapp_flutter/src/offlineProviders/SharedPreferencesProvider.dart';

String userIdKey = 'userId';

String restaurantIdStatisticsKey = 'restaurantIdStatistics';
String restaurantNameStatisticsKey = 'restaurantNameStatistics';
String restaurantAddressStatisticsKey = 'restaurantAddressStatistics';
String restaurantCategoryStatisticsKey = 'restaurantCategoryStatistics';

String favoriteRestaurantIdKey = 'favoriteRestaurantId';
String favoriteRestaurantNameKey = 'favoriteRestaurantName';
String favoriteRestaurantAddressKey = 'favoriteRestaurantAddress';
String favoriteRestaurantCategoryKey = 'favoriteRestaurantCategory';

String favoriteRestaurantIdCountKey = 'favoriteRestaurantIdCount';
String favoriteRestaurantNameCountKey = 'favoriteRestaurantNameCount';
String favoriteRestaurantAddressCountKey = 'favoriteRestaurantAddressCount';
String favoriteRestaurantCategoryCountKey = 'favoriteRestaurantCategoryCount';

class UserActionStatistics {
  String userId;
  Map<String, int> restaurantIdStatistics;
  Map<String, int> restaurantNameStatistics;
  Map<String, int> restaurantAddressStatistics;
  Map<String, int> restaurantCategoryStatistics;

  String favoriteRestaurantId;
  String favoriteRestaurantName;
  String favoriteRestaurantAddress;
  String favoriteRestaurantCategory;

  int favoriteRestaurantIdCount = 0;
  int favoriteRestaurantNameCount = 0;
  int favoriteRestaurantAddressCount = 0;
  int favoriteRestaurantCategoryCount = 0;

  UserActionStatistics({
    this.userId,
    this.restaurantIdStatistics,
    this.restaurantNameStatistics,
    this.restaurantAddressStatistics,
    this.restaurantCategoryStatistics,
    this.favoriteRestaurantId,
    this.favoriteRestaurantName,
    this.favoriteRestaurantAddress,
    this.favoriteRestaurantCategory,
    this.favoriteRestaurantIdCount: 0,
    this.favoriteRestaurantNameCount: 0,
    this.favoriteRestaurantAddressCount: 0,
    this.favoriteRestaurantCategoryCount: 0,
  }) {
    restaurantIdStatistics =
        restaurantIdStatistics == null ? {} : restaurantIdStatistics;
    restaurantNameStatistics =
        restaurantNameStatistics == null ? {} : restaurantNameStatistics;
    restaurantAddressStatistics =
        restaurantAddressStatistics == null ? {} : restaurantAddressStatistics;
    restaurantCategoryStatistics = restaurantCategoryStatistics == null
        ? {}
        : restaurantCategoryStatistics;
  }

  factory UserActionStatistics.fromFirestore(
    QueryDocumentSnapshot userActionStatisticsDoc,
  ) {
    return UserActionStatistics(
      userId: userActionStatisticsDoc[favoriteRestaurantIdKey],
      favoriteRestaurantId: userActionStatisticsDoc[userIdKey],
      restaurantIdStatistics: Map<String, int>.from(
        userActionStatisticsDoc[restaurantIdStatisticsKey],
      ),
      restaurantNameStatistics: Map<String, int>.from(
        userActionStatisticsDoc[restaurantNameStatisticsKey],
      ),
      restaurantAddressStatistics: Map<String, int>.from(
        userActionStatisticsDoc[restaurantAddressStatisticsKey],
      ),
      restaurantCategoryStatistics: Map<String, int>.from(
        userActionStatisticsDoc[restaurantCategoryStatisticsKey],
      ),
      favoriteRestaurantName:
          userActionStatisticsDoc[favoriteRestaurantNameKey],
      favoriteRestaurantAddress:
          userActionStatisticsDoc[favoriteRestaurantAddressKey],
      favoriteRestaurantCategory:
          userActionStatisticsDoc[favoriteRestaurantCategoryKey],
      favoriteRestaurantIdCount:
          userActionStatisticsDoc[favoriteRestaurantIdCountKey],
      favoriteRestaurantNameCount:
          userActionStatisticsDoc[favoriteRestaurantNameCountKey],
      favoriteRestaurantAddressCount:
          userActionStatisticsDoc[favoriteRestaurantAddressCountKey],
      favoriteRestaurantCategoryCount:
          userActionStatisticsDoc[favoriteRestaurantCategoryCountKey],
    );
  }

  void processUserActions(
    List<UserAction> userActions,
  ) {
    for (var i = 0; i < userActions.length; i++) {
      if (userActions[i].restaurantId != null) {
        processRestaurantId(userActions[i].restaurantId);
      }
      if (userActions[i].restaurantName != null) {
        processRestaurantName(userActions[i].restaurantName);
      }
      if (userActions[i].restaurantAddress != null) {
        processRestaurantAddress(userActions[i].restaurantAddress);
      }
      if (userActions[i].restaurantCategory != null) {
        processRestaurantCategory(userActions[i].restaurantCategory);
      }
    }
  }

  void processRestaurantId(String restaurantId) {
    if (restaurantIdStatistics[restaurantId] == null) {
      restaurantIdStatistics[restaurantId] = 1;
    } else {
      restaurantIdStatistics[restaurantId] += 1;
    }

    if (restaurantIdStatistics[restaurantId] > favoriteRestaurantIdCount) {
      favoriteRestaurantId = restaurantId;
      favoriteRestaurantIdCount = restaurantIdStatistics[restaurantId];
    }
  }

  void processRestaurantName(String restaurantName) {
    if (restaurantNameStatistics[restaurantName] == null) {
      restaurantNameStatistics[restaurantName] = 1;
    } else {
      restaurantNameStatistics[restaurantName] += 1;
    }

    if (restaurantNameStatistics[restaurantName] >
        favoriteRestaurantNameCount) {
      favoriteRestaurantName = restaurantName;
      favoriteRestaurantNameCount = restaurantNameStatistics[restaurantName];
    }
  }

  void processRestaurantAddress(String restaurantAddress) {
    if (restaurantAddressStatistics[restaurantAddress] == null) {
      restaurantAddressStatistics[restaurantAddress] = 1;
    } else {
      restaurantAddressStatistics[restaurantAddress] += 1;
    }

    if (restaurantAddressStatistics[restaurantAddress] >
        favoriteRestaurantAddressCount) {
      favoriteRestaurantAddress = restaurantAddress;
      favoriteRestaurantAddressCount =
          restaurantAddressStatistics[restaurantAddress];
    }
  }

  void processRestaurantCategory(String restaurantCategory) {
    if (restaurantCategoryStatistics[restaurantCategory] == null) {
      restaurantCategoryStatistics[restaurantCategory] = 1;
    } else {
      restaurantCategoryStatistics[restaurantCategory] += 1;
    }

    if (restaurantCategoryStatistics[restaurantCategory] >
        favoriteRestaurantCategoryCount) {
      favoriteRestaurantCategory = restaurantCategory;
      favoriteRestaurantCategoryCount =
          restaurantCategoryStatistics[restaurantCategory];
    }
  }

  String toString() => '''
      $favoriteRestaurantIdKey: $favoriteRestaurantId ($favoriteRestaurantIdCount)\n
      $favoriteRestaurantNameKey: $favoriteRestaurantName ($favoriteRestaurantNameCount)\n
      $favoriteRestaurantAddressKey: $favoriteRestaurantAddress ($favoriteRestaurantAddressCount)\n
      $favoriteRestaurantCategoryKey: $favoriteRestaurantCategory ($favoriteRestaurantCategoryCount)\n
      ${restaurantIdStatistics.toString()}\n
      ${restaurantNameStatistics.toString()}\n
      ${restaurantAddressStatistics.toString()}\n
      ${restaurantCategoryStatistics.toString()}\n
      ''';

  Map<String, dynamic> toFirebaseDoc() => {
        restaurantIdStatisticsKey: restaurantIdStatistics,
        restaurantNameStatisticsKey: restaurantNameStatistics,
        restaurantAddressStatisticsKey: restaurantAddressStatistics,
        restaurantCategoryStatisticsKey: restaurantCategoryStatistics,
        favoriteRestaurantIdKey: favoriteRestaurantId,
        favoriteRestaurantNameKey: favoriteRestaurantName,
        favoriteRestaurantAddressKey: favoriteRestaurantAddress,
        favoriteRestaurantCategoryKey: favoriteRestaurantCategory,
        favoriteRestaurantIdCountKey: favoriteRestaurantIdCount,
        favoriteRestaurantNameCountKey: favoriteRestaurantNameCount,
        favoriteRestaurantAddressCountKey: favoriteRestaurantAddressCount,
        favoriteRestaurantCategoryCountKey: favoriteRestaurantCategoryCount,
      };

  Map<String, dynamic> toSharedPreferencesMap() => {
        restaurantIdStatisticsKey: SharedPreferencesProvider.mapToStringList(
          restaurantIdStatistics,
        ),
        restaurantNameStatisticsKey: SharedPreferencesProvider.mapToStringList(
          restaurantNameStatistics,
        ),
        restaurantAddressStatisticsKey:
            SharedPreferencesProvider.mapToStringList(
          restaurantAddressStatistics,
        ),
        restaurantCategoryStatisticsKey:
            SharedPreferencesProvider.mapToStringList(
          restaurantCategoryStatistics,
        ),
        favoriteRestaurantIdKey: favoriteRestaurantId,
        favoriteRestaurantNameKey: favoriteRestaurantName,
        favoriteRestaurantAddressKey: favoriteRestaurantAddress,
        favoriteRestaurantCategoryKey: favoriteRestaurantCategory,
        favoriteRestaurantIdCountKey: favoriteRestaurantIdCount,
        favoriteRestaurantNameCountKey: favoriteRestaurantNameCount,
        favoriteRestaurantAddressCountKey: favoriteRestaurantAddressCount,
        favoriteRestaurantCategoryCountKey: favoriteRestaurantCategoryCount,
      };
}
