import 'package:flutter/material.dart';

class StyledTitle extends StatelessWidget {
  final String text;
  final double fontSize;
  final TextAlign textAlign;

  StyledTitle({
    this.text,
    this.fontSize: 20,
    this.textAlign: TextAlign.start,
  });

  @override
  Widget build(BuildContext context) {
    return Text(
      text,
      textAlign: textAlign,
      style: TextStyle(
        fontSize: fontSize,
        color: Colors.red[900],
        fontWeight: FontWeight.bold,
        decoration: TextDecoration.none,
      ),
    );
  }
}
