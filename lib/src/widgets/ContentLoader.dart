import 'package:flutter/material.dart';

class ContentLoader extends StatelessWidget {
  final Future future;
  final Widget Function(dynamic) childBuilder;
  final double loadScreenHeight;

  ContentLoader({this.future, this.childBuilder, this.loadScreenHeight: 0.0});

  @override
  Widget build(BuildContext context) {
    return Container(
      margin: EdgeInsets.all(20.0),
      child: FutureBuilder(
        future: future,
        builder: (BuildContext context, AsyncSnapshot snapshot) {
          if (snapshot.hasData) {
            return childBuilder(snapshot.data);
          } else {
            return ConstrainedBox(
              constraints: new BoxConstraints(
                minHeight: loadScreenHeight,
              ),
              child: Center(
                child: CircularProgressIndicator(),
              ),
            );
          }
        },
      ),
    );
  }
}
