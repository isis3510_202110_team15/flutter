import 'package:flutter/material.dart';

class SearchBar extends StatelessWidget {
  final Function filterCategories;
  final String placeholder;

  SearchBar({
    @required this.filterCategories,
    this.placeholder = 'Buscar',
  });

  @override
  Widget build(BuildContext context) {
    return Container(
      padding: EdgeInsets.only(left: 10.0),
      decoration: BoxDecoration(
        border: Border.all(
          color: Colors.white,
        ),
        borderRadius: new BorderRadius.circular(10.0),
        color: Colors.white,
      ),
      margin: const EdgeInsets.all(10.0),
      child: TextField(
        onChanged: (value) {
          filterCategories(value);
        },
        style: TextStyle(color: Colors.grey[800]),
        decoration: InputDecoration(
          icon: Icon(
            Icons.search,
            color: Colors.grey[800],
          ),
          hintText: placeholder,
          hintStyle: TextStyle(color: Colors.grey[800]),
          border: InputBorder.none,
          focusedBorder: InputBorder.none,
          enabledBorder: InputBorder.none,
          errorBorder: InputBorder.none,
          disabledBorder: InputBorder.none,
        ),
      ),
    );
  }
}
