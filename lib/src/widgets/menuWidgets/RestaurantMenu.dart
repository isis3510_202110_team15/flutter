import 'package:flutter/material.dart';
import 'package:reservapp_flutter/src/model/Restaurant.dart';
import 'package:reservapp_flutter/src/widgets/StyledTitle.dart';
import 'package:reservapp_flutter/src/widgets/menuWidgets/MenuBody/MenuBody.dart';

class RestaurantMenu extends StatelessWidget {
  final Restaurant restaurant;

  RestaurantMenu(this.restaurant);

  @override
  Widget build(BuildContext context) {
    return Column(
      children: <Widget>[
        Text(
          'Menu',
          style: TextStyle(
            color: Colors.grey[800],
            fontSize: 15.0,
            fontWeight: FontWeight.bold,
            decoration: TextDecoration.none,
          ),
        ),
        SizedBox(height: 10),
        StyledTitle(
          text: restaurant.name,
          textAlign: TextAlign.center,
        ),
        MenuBody(restaurant.id),
      ],
    );
  }
}
