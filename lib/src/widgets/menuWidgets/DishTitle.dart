import 'package:flutter/material.dart';
import 'package:reservapp_flutter/src/model/Dish.dart';

import 'package:top_snackbar_flutter/custom_snack_bar.dart';
import 'package:top_snackbar_flutter/top_snack_bar.dart';

class DishTitle extends StatelessWidget {
  final Dish dish;
  final bool hasAllergicIngredients;

  DishTitle({
    this.dish,
    this.hasAllergicIngredients,
  });

  @override
  Widget build(BuildContext context) {
    return Row(
      mainAxisAlignment: MainAxisAlignment.start,
      children: <Widget>[
        hasAllergicIngredients
            ? GestureDetector(
                onTap: () {
                  showTopSnackBar(
                    context,
                    CustomSnackBar.error(
                      message:
                          'This dish contains one or more ingredients you are allergic to.',
                    ),
                  );
                },
                child: Image(
                  image: AssetImage('assets/alert.png'),
                  width: 15.0,
                  height: 15.0,
                ),
              )
            : Container(),
        hasAllergicIngredients ? SizedBox(width: 5.0) : Container(),
        Text(
          dish.name,
          style: TextStyle(
            color:
                hasAllergicIngredients ? Colors.yellow[700] : Colors.grey[800],
            fontSize: 15.0,
            decoration: TextDecoration.none,
          ),
        ),
      ],
    );
  }
}
