import 'package:flutter/material.dart';
import 'package:reservapp_flutter/src/model/Menu.dart';
import 'package:reservapp_flutter/src/providers/AllergiesProvider.dart';
import 'package:reservapp_flutter/src/providers/MenusProvider.dart';
import 'package:reservapp_flutter/src/widgets/ContentLoader.dart';
import 'package:reservapp_flutter/src/widgets/banners/NoConnectivityBanner.dart';
import 'package:reservapp_flutter/src/widgets/menuWidgets/MenuBody/AfternoonMenuBody.dart';
import 'package:reservapp_flutter/src/widgets/menuWidgets/MenuBody/MorningMenuBody.dart';

enum MENU_TYPE {
  MORNING,
  AFTERNOON,
}

class MenuBody extends StatefulWidget {
  final String restaurantId;

  MenuBody(this.restaurantId);

  @override
  _MenuBodyState createState() => _MenuBodyState(restaurantId);
}

class _MenuBodyState extends State<MenuBody> {
  final String restaurantId;
  Map<String, bool> _allergies = {};
  MENU_TYPE _menuType;

  _MenuBodyState(this.restaurantId) {
    DateTime now = DateTime.now();
    if (now.hour > 4 && now.hour < 12) {
      _menuType = MENU_TYPE.MORNING;
    } else {
      _menuType = MENU_TYPE.AFTERNOON;
    }
  }

  @override
  void initState() {
    AllergiesProvider.getAllergies(context).then(
      (allergies) {
        setState(() {
          _allergies = allergies;
        });
      },
    );
    super.initState();
  }

  @override
  Widget build(BuildContext context) {
    return ContentLoader(
      future: MenusProvider.getMenu(context, restaurantId),
      childBuilder: (menu) {
        return SizedBox(
          child: ListView(
            children: <Widget>[
              Text(
                _menuType == MENU_TYPE.MORNING
                    ? 'Morning Menu'
                    : 'Afternoon Menu',
                textAlign: TextAlign.center,
                style: TextStyle(
                  fontSize: 22.0,
                  color: Colors.red[900],
                  fontWeight: FontWeight.bold,
                  decoration: TextDecoration.none,
                ),
              ),
              GestureDetector(
                onTap: () {
                  final menuType = _menuType == MENU_TYPE.MORNING
                      ? MENU_TYPE.AFTERNOON
                      : MENU_TYPE.MORNING;
                  setState(() {
                    _menuType = menuType;
                  });
                },
                child: Text(
                  _menuType == MENU_TYPE.MORNING
                      ? 'Go to afternoon menu ->'
                      : 'Go to morning menu ->',
                  textAlign: TextAlign.center,
                  style: TextStyle(
                    fontSize: 12.0,
                    color: Colors.red[900],
                    fontWeight: FontWeight.bold,
                    decoration: TextDecoration.none,
                  ),
                ),
              ),
              SizedBox(height: 15.0),
              menu == null
                  ? NoConnectivityBanner()
                  : _menuType == MENU_TYPE.MORNING
                      ? MorningMenuBody(
                          menu: menu as Menu,
                          allergies: _allergies,
                        )
                      : AfternoonMenuBody(
                          menu: menu as Menu,
                          allergies: _allergies,
                        )
            ],
          ),
        );
      },
    );
  }
}
