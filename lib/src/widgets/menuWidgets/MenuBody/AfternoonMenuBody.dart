import 'package:flutter/material.dart';
import 'package:reservapp_flutter/src/model/Menu.dart';
import 'package:reservapp_flutter/src/widgets/StyledTitle.dart';
import 'package:reservapp_flutter/src/widgets/menuWidgets/DishItemList.dart';

class AfternoonMenuBody extends StatelessWidget {
  final Menu menu;
  final Map<String, bool> allergies;

  AfternoonMenuBody({
    this.menu,
    this.allergies,
  });

  @override
  Widget build(BuildContext context) {
    return Column(
      children: <Widget>[
        StyledTitle(
          text: 'Drinks',
          fontSize: 16,
        ),
        DishItemList(
          dishes: menu.drinks,
          allergies: allergies,
        ),
        StyledTitle(
          text: 'Entrees',
          fontSize: 16,
        ),
        DishItemList(
          dishes: menu.entrees,
          allergies: allergies,
        ),
        StyledTitle(
          text: 'Mains',
          fontSize: 16,
        ),
        DishItemList(
          dishes: menu.mains,
          allergies: allergies,
        ),
        StyledTitle(
          text: 'Deserts',
          fontSize: 16,
        ),
        DishItemList(
          dishes: menu.desserts,
          allergies: allergies,
        ),
      ],
    );
  }
}
