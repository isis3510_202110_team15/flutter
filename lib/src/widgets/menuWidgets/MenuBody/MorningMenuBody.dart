import 'package:flutter/material.dart';
import 'package:reservapp_flutter/src/model/Menu.dart';
import 'package:reservapp_flutter/src/widgets/StyledTitle.dart';
import 'package:reservapp_flutter/src/widgets/menuWidgets/DishItemList.dart';

class MorningMenuBody extends StatelessWidget {
  final Menu menu;
  final Map<String, bool> allergies;

  MorningMenuBody({
    this.menu,
    this.allergies,
  });

  @override
  Widget build(BuildContext context) {
    return Column(children: <Widget>[
      StyledTitle(
        text: 'Drinks',
        fontSize: 16,
      ),
      DishItemList(
        dishes: menu.drinks,
        allergies: allergies,
      ),
      StyledTitle(
        text: 'Breakfasts',
        fontSize: 16,
      ),
      DishItemList(
        dishes: menu.breakfasts,
        allergies: allergies,
      ),
    ]);
  }
}
