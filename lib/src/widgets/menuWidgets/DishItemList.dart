import 'package:flutter/material.dart';
import 'package:reservapp_flutter/src/model/Dish.dart';
import 'package:reservapp_flutter/src/widgets/menuWidgets/DishItem.dart';

import 'DishItem.dart';

class DishItemList extends StatelessWidget {
  final List<Dish> dishes;
  final Map<String, bool> allergies;

  DishItemList({
    this.dishes,
    this.allergies,
  });

  Widget getDishItems() {
    List<Widget> dishWidgets = [];

    bool hasAlergicIngredients = false;
    for (var i = 0; i < dishes.length; i++) {
      for (var j = 0; j < dishes[i].ingredients.length; j++) {
        if (allergies[dishes[i].ingredients[j]] == true) {
          hasAlergicIngredients = true;
        }
      }
      dishWidgets.add(DishItem(
        dish: dishes[i],
        hasAllergicIngredients: hasAlergicIngredients,
      ));
    }

    return Column(
      children: dishWidgets,
    );
  }

  @override
  Widget build(BuildContext context) {
    return Container(
      padding: EdgeInsets.only(top: 10.0),
      child: dishes.length > 0
          ? getDishItems()
          : Text(
              '----------------------',
              style: TextStyle(
                fontSize: 25.0,
                color: Colors.grey[900],
                decoration: TextDecoration.none,
              ),
            ),
    );
  }
}
