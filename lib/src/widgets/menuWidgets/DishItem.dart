import 'package:flutter/material.dart';
import 'package:reservapp_flutter/src/model/Dish.dart';
import 'package:reservapp_flutter/src/widgets/menuWidgets/DishTitle.dart';

class DishItem extends StatelessWidget {
  final Dish dish;
  final bool hasAllergicIngredients;

  DishItem({
    this.dish,
    this.hasAllergicIngredients,
  });

  @override
  Widget build(BuildContext context) {
    return Container(
      padding: EdgeInsets.only(top: 10.0),
      child: Column(
        crossAxisAlignment: CrossAxisAlignment.start,
        children: <Widget>[
          Row(
            mainAxisAlignment: MainAxisAlignment.spaceBetween,
            children: <Widget>[
              DishTitle(
                dish: dish,
                hasAllergicIngredients: hasAllergicIngredients,
              ),
              Text(
                '\$${dish.price.toStringAsFixed(2)}',
                style: TextStyle(
                  color: Colors.grey[800],
                  fontSize: 15.0,
                  decoration: TextDecoration.none,
                ),
              ),
            ],
          ),
          Text(
            dish.ingredients.join(', '),
            style: TextStyle(
              color: Colors.grey[600],
              fontSize: 12.0,
              decoration: TextDecoration.none,
            ),
          ),
        ],
      ),
    );
  }
}
