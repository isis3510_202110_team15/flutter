import 'package:flutter/material.dart';
import 'package:cached_network_image/cached_network_image.dart';

class ItemImage extends StatelessWidget {
  final String imageUrl;
  final double size;

  ItemImage({
    this.imageUrl,
    this.size: 0.1,
  });

  @override
  Widget build(BuildContext context) {
    return Container(
      margin: const EdgeInsets.only(
        right: 15.0,
      ),
      height: MediaQuery.of(context).size.height * size,
      width: MediaQuery.of(context).size.height * size,
      child: CachedNetworkImage(
        placeholder: (context, url) => Container(
          margin: EdgeInsets.all(20.0),
          child: CircularProgressIndicator(),
        ),
        imageUrl: imageUrl,
      ),
    );
  }
}
