import 'package:flutter/material.dart';
import 'package:reservapp_flutter/src/widgets/appBarWidgets/BarcodeScannerButton.dart';
import 'package:reservapp_flutter/src/widgets/appBarWidgets/MenuDrawer.dart';

class ViewContainer extends StatefulWidget {
  final String title;
  final Widget child;

  ViewContainer({
    this.title,
    this.child,
  });

  @override
  _ViewContainerState createState() => _ViewContainerState(
        title: title,
        child: child,
      );
}

class _ViewContainerState extends State<ViewContainer> {
  final String title;
  final Widget child;

  _ViewContainerState({
    this.title,
    this.child,
  });

  @override
  Widget build(
    BuildContext context,
  ) {
    return Material(
      child: Scaffold(
        backgroundColor: Colors.grey,
        appBar: AppBar(
          title: Text(title),
          actions: <Widget>[
            BarcodeScannerButton(),
          ],
        ),
        drawer: MenuDrawer(),
        body: child,
      ),
    );
  }
}
