import 'package:flutter/material.dart';

class CardContainer extends StatelessWidget {
  final Widget child;

  CardContainer({this.child});

  @override
  Widget build(BuildContext context) {
    return Container(
      margin: EdgeInsets.only(
        top: MediaQuery.of(context).size.height * 0.07,
        bottom: MediaQuery.of(context).size.height * 0.07,
        left: MediaQuery.of(context).size.height * 0.02,
        right: MediaQuery.of(context).size.height * 0.02,
      ),
      padding: EdgeInsets.only(
        top: 10.0,
        bottom: 10.0,
      ),
      decoration: new BoxDecoration(
        borderRadius: new BorderRadius.circular(16.0),
        color: Colors.white,
      ),
      child: child,
    );
  }
}
