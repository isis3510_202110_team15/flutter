import 'package:flutter/material.dart';

class FieldContainer extends StatelessWidget {
  final String text;
  final double fontSize;
  final TextAlign textAlign;

  FieldContainer({
    this.text,
    this.fontSize: 25.0,
    this.textAlign: TextAlign.center,
  });

  @override
  Widget build(BuildContext context) {
    return Container(
      margin: EdgeInsets.only(right: 5),
      padding: EdgeInsets.only(
        right: 10,
        left: 10,
        top: 5,
        bottom: 5,
      ),
      decoration: BoxDecoration(
        borderRadius: new BorderRadius.circular(10.0),
        color: Colors.grey[300],
      ),
      child: Text(
        text,
        textAlign: textAlign,
        style: TextStyle(
          fontSize: fontSize,
          color: Colors.black,
          fontWeight: FontWeight.bold,
          decoration: TextDecoration.none,
        ),
      ),
    );
  }
}
