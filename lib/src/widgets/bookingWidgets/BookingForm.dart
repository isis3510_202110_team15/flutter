import 'package:flutter/material.dart';
import 'package:reservapp_flutter/src/model/Booking.dart';
import 'package:reservapp_flutter/src/widgets/ItemImage.dart';
import 'package:reservapp_flutter/src/widgets/ScrollViewWrapper.dart';
import 'package:reservapp_flutter/src/widgets/StyledTitle.dart';
import 'package:reservapp_flutter/src/widgets/bookingWidgets/DateSelector.dart';
import 'package:reservapp_flutter/src/widgets/bookingWidgets/PeopleQuantitySelector.dart';

class BookingForm extends StatefulWidget {
  final Booking booking;
  final Function saveBooking;
  final String title;

  BookingForm({
    this.booking,
    this.saveBooking,
    this.title,
  });

  @override
  _BookingFormState createState() => _BookingFormState(
        booking: booking,
        saveBooking: saveBooking,
        title: title,
      );
}

class _BookingFormState extends State<BookingForm> {
  final Booking booking;
  final Function saveBooking;
  final String title;

  _BookingFormState({
    this.booking,
    this.saveBooking,
    this.title,
  });

  @override
  Widget build(BuildContext context) {
    return Container(
      padding: EdgeInsets.all(20),
      child: ScrollViewWrapper(
        children: <Widget>[
          Center(
            child: ItemImage(
              imageUrl: booking.image,
              size: 0.2,
            ),
          ),
          SizedBox(height: 10),
          Center(
            child: Text(
              title,
              textAlign: TextAlign.center,
              style: TextStyle(
                fontSize: 25.0,
                color: Colors.grey[900],
                fontWeight: FontWeight.bold,
                decoration: TextDecoration.none,
              ),
            ),
          ),
          SizedBox(height: 10),
          StyledTitle(
            text: 'Date:',
            fontSize: 16.0,
          ),
          DateSelector(
            initialDate: booking.date,
            setSelectedDate: (date) => booking.date = date,
          ),
          SizedBox(height: 20),
          StyledTitle(
            text: 'People quantity:',
            fontSize: 16.0,
          ),
          PeopleQuantitySelector(
            initialPeopleQuantity: booking.peopleQuantity,
            setSelectedPeopleQuantity: (peopleQuantity) =>
                booking.peopleQuantity = peopleQuantity,
          ),
          SizedBox(height: 30.0),
          Center(
            child: MaterialButton(
              child: Text(
                'Enviar',
                style: TextStyle(
                  fontSize: 25.0,
                  color: Colors.white,
                  decoration: TextDecoration.none,
                ),
              ),
              onPressed: saveBooking,
              color: Colors.red[700],
            ),
          ),
        ],
      ),
    );
  }
}
