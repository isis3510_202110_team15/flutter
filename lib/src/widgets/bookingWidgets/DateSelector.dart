import 'package:flutter/material.dart';
import 'package:flutter_datetime_picker/flutter_datetime_picker.dart';
import 'package:jiffy/jiffy.dart';
import 'package:reservapp_flutter/src/widgets/StyledTitle.dart';
import 'package:reservapp_flutter/src/widgets/containers/FieldContainer.dart';

class DateSelector extends StatefulWidget {
  final DateTime initialDate;
  final Function setSelectedDate;

  DateSelector({this.initialDate, this.setSelectedDate});

  @override
  _DateSelectorState createState() => _DateSelectorState(
        initialDate: initialDate,
        setSelectedDate: setSelectedDate,
      );
}

class _DateSelectorState extends State<DateSelector> {
  final Function setSelectedDate;
  final DateTime initialDate;
  DateTime _date;

  _DateSelectorState({
    this.initialDate,
    this.setSelectedDate,
  }) {
    _date = initialDate;
  }

  void setDate(DateTime date) {
    setState(() {
      _date = date;
    });
    setSelectedDate(date);
  }

  void _showDatePicker(context) {
    DateTime now = DateTime.now();
    final minTime = Jiffy(now).add(hours: 1);
    final maxTime = Jiffy(now).add(months: 1);
    if (_date.isBefore(minTime)) {
      setState(() {
        _date = minTime;
      });
    }

    DatePicker.showDateTimePicker(
      context,
      showTitleActions: true,
      minTime: minTime,
      maxTime: maxTime,
      onChanged: (date) {},
      onConfirm: setDate,
      currentTime: _date,
      locale: LocaleType.es,
    );
  }

  @override
  Widget build(BuildContext context) {
    return GestureDetector(
      onTap: () {
        _showDatePicker(context);
      },
      child: Container(
        child: Column(
          crossAxisAlignment: CrossAxisAlignment.start,
          children: <Widget>[
            Row(
              children: <Widget>[
                FieldContainer(
                  text: '${_date.day <= 9 ? '0' : ''}${_date.day.toString()}',
                ),
                FieldContainer(
                  text:
                      '${_date.month <= 9 ? '0' : ''}${_date.month.toString()}',
                ),
                FieldContainer(
                  text: _date.year.toString(),
                ),
              ],
            ),
            SizedBox(height: 10.0),
            StyledTitle(
              text: 'Hour:',
              fontSize: 16.0,
            ),
            FieldContainer(
              text:
                  '${_date.hour <= 9 ? '0' : ''}${_date.hour.toString()}:${_date.minute <= 9 ? '0' : ''}${_date.minute}',
            ),
          ],
        ),
      ),
    );
  }
}
