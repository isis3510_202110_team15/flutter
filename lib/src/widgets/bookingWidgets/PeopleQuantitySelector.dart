import 'package:flutter/material.dart';
import 'package:reservapp_flutter/src/widgets/containers/FieldContainer.dart';
import 'package:top_snackbar_flutter/custom_snack_bar.dart';
import 'package:top_snackbar_flutter/top_snack_bar.dart';

class PeopleQuantitySelector extends StatefulWidget {
  final int initialPeopleQuantity;
  final Function setSelectedPeopleQuantity;
  PeopleQuantitySelector({
    this.initialPeopleQuantity,
    this.setSelectedPeopleQuantity,
  });

  @override
  _PeopleQuantitySelectorState createState() => _PeopleQuantitySelectorState(
        initialPeopleQuantity: initialPeopleQuantity,
        setSelectedPeopleQuantity: setSelectedPeopleQuantity,
      );
}

class _PeopleQuantitySelectorState extends State<PeopleQuantitySelector> {
  final int initialPeopleQuantity;
  final Function setSelectedPeopleQuantity;
  int _peopleQuantity = 2;

  _PeopleQuantitySelectorState({
    this.initialPeopleQuantity,
    this.setSelectedPeopleQuantity,
  }) {
    _peopleQuantity = initialPeopleQuantity;
  }

  void setPeopleQuantity(int peopleQuantity) {
    if (peopleQuantity > 20) {
      showTopSnackBar(
        context,
        CustomSnackBar.info(
          message: 'Maximum of 20 people reservations',
        ),
      );
    } else if (peopleQuantity <= 0) {
      showTopSnackBar(
        context,
        CustomSnackBar.info(
          message: 'Minimum of one person to book a resrvation.',
        ),
      );
    } else {
      setState(() {
        _peopleQuantity = peopleQuantity;
      });
      setSelectedPeopleQuantity(peopleQuantity);
    }
  }

  @override
  Widget build(BuildContext context) {
    return Container(
      height: 50.0,
      child: Row(
        children: <Widget>[
          GestureDetector(
            onTap: () {
              setPeopleQuantity(_peopleQuantity - 1);
            },
            child: FieldContainer(
              text: '<',
            ),
          ),
          FieldContainer(
            text: _peopleQuantity.toString(),
          ),
          GestureDetector(
            onTap: () {
              setPeopleQuantity(_peopleQuantity + 1);
            },
            child: FieldContainer(
              text: '>',
            ),
          ),
        ],
      ),
    );
  }
}
