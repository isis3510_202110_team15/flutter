import 'package:flutter/material.dart';
import 'package:reservapp_flutter/src/model/Booking.dart';
import 'package:reservapp_flutter/src/widgets/bookingWidgets/BookingItem/BookingItem.dart';

class BookingsList extends StatelessWidget {
  final List<Booking> bookings;

  BookingsList(this.bookings);

  @override
  Widget build(BuildContext context) {
    return Container(
      padding: EdgeInsets.all(10),
      child: bookings.length > 0
          ? ListView.builder(
              itemCount: bookings.length,
              itemBuilder: (BuildContext context, int index) {
                return BookingItem(bookings[index]);
              },
            )
          : Text(
              'You have no bookings yet :(',
              style: TextStyle(
                fontSize: 20,
                fontWeight: FontWeight.bold,
              ),
            ),
    );
  }
}
