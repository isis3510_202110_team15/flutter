import 'package:flutter/material.dart';
import 'package:reservapp_flutter/src/model/Booking.dart';
import 'package:reservapp_flutter/src/model/UserAction.dart';
import 'package:reservapp_flutter/src/providers/BookingsProvider.dart';
import 'package:reservapp_flutter/src/providers/UserActionsProvider.dart';
import 'package:reservapp_flutter/src/widgets/StyledTitle.dart';
import 'package:reservapp_flutter/src/widgets/banners/NoConnectivityBanner.dart';
import 'package:reservapp_flutter/src/widgets/bookingWidgets/BookingForm.dart';

class EditBookingView extends StatefulWidget {
  final Booking booking;

  EditBookingView(this.booking);

  @override
  _EditBookingViewState createState() => _EditBookingViewState(booking);
}

enum STATE {
  fillingForm,
  loading,
  done,
  noConnectivity,
}

class _EditBookingViewState extends State<EditBookingView> {
  Booking _booking;
  STATE _state = STATE.fillingForm;

  _EditBookingViewState(Booking booking) {
    _booking = booking;
  }

  void patchBooking() async {
    if (_state == STATE.fillingForm) {
      setState(() {
        _state = STATE.loading;
      });

      Booking booking = await BookingsProvider.patchBooking(context, _booking);
      if (booking != null) {
        UserActionsProvider.postUserActionFromBooking(
          context,
          _booking,
          USER_ACTION_TYPES.editBooking,
          VIEWS.bookingsDialog,
        );

        setState(() {
          _state = STATE.done;
        });
      } else {
        setState(() {
          _state = STATE.noConnectivity;
        });
      }
    }
  }

  @override
  Widget build(BuildContext context) {
    if (_state == STATE.noConnectivity) {
      return NoConnectivityBanner(
        text: 'You can edit your booking once you have connection!',
      );
    }

    if (_state == STATE.fillingForm) {
      return BookingForm(
        booking: _booking,
        saveBooking: patchBooking,
        title: 'Edit your booking',
      );
    }

    if (_state == STATE.done) {
      return Column(
        children: <Widget>[
          SizedBox(height: 50),
          StyledTitle(
            text: 'We have received the updates ;)',
            textAlign: TextAlign.center,
          ),
          Container(
            margin: EdgeInsets.all(15.0),
            child: Image.asset('assets/bookings.jpg'),
          ),
          MaterialButton(
            child: Text(
              'Return to bookings',
              style: TextStyle(
                fontSize: 25.0,
                color: Colors.white,
                decoration: TextDecoration.none,
              ),
            ),
            onPressed: () {
              Navigator.of(context).pop();
            },
            color: Colors.red[700],
          ),
        ],
      );
    }

    return Center(
      child: CircularProgressIndicator(),
    );
  }
}
