import 'package:flutter/material.dart';
import 'package:reservapp_flutter/src/model/Booking.dart';
import 'package:reservapp_flutter/src/widgets/ItemImage.dart';
import 'package:reservapp_flutter/src/widgets/bookingWidgets/BookingItem/BookingInfo.dart';

class BookingItem extends StatelessWidget {
  final Booking booking;

  BookingItem(this.booking);

  @override
  Widget build(BuildContext context) {
    return Card(
      elevation: 10,
      color: booking.isPast() ? Colors.grey[400] : Colors.white,
      margin: const EdgeInsets.only(
        bottom: 20.0,
      ),
      child: Padding(
        padding: const EdgeInsets.symmetric(vertical: 10, horizontal: 8),
        child: Row(
          mainAxisAlignment: MainAxisAlignment.spaceBetween,
          children: <Widget>[
            ItemImage(
              imageUrl: booking.image,
            ),
            BookingInfo(booking),
          ],
        ),
      ),
    );
  }
}
