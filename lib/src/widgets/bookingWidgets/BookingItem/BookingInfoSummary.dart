import 'package:flutter/material.dart';
import 'package:reservapp_flutter/src/model/Booking.dart';

class BookingInfoSummary extends StatelessWidget {
  final Booking booking;

  BookingInfoSummary(this.booking);

  @override
  Widget build(BuildContext context) {
    return Column(
      crossAxisAlignment: CrossAxisAlignment.start,
      children: <Widget>[
        Text(
          '${booking.date.hour <= 9 ? '0' : ''}${booking.date.hour.toString()}:${booking.date.minute <= 9 ? '0' : ''}${booking.date.minute}',
          style: TextStyle(fontSize: 15),
        ),
        Text(
          '${booking.date.day <= 9 ? '0' : ''}${booking.date.day.toString()}-${booking.date.month <= 9 ? '0' : ''}${booking.date.month.toString()}-${booking.date.year}',
          style: TextStyle(fontSize: 15),
        ),
        Text(
          'Mesa para ${booking.peopleQuantity.toString()}',
          style: TextStyle(fontSize: 15),
        ),
        Text(
          booking.restaurantAddress,
          style: TextStyle(fontSize: 13),
        ),
      ],
    );
  }
}
