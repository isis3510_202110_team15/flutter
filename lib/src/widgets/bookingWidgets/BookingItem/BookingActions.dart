import 'package:flutter/material.dart';
import 'package:reservapp_flutter/src/model/Booking.dart';
import 'package:reservapp_flutter/src/model/UserAction.dart';
import 'package:reservapp_flutter/src/providers/UserActionsProvider.dart';
import 'package:reservapp_flutter/src/widgets/DialogButton.dart';
import 'package:reservapp_flutter/src/widgets/containers/CardContainer.dart';
import 'package:reservapp_flutter/src/widgets/dialogs/MenuBookingDialog/BookingActionsDialog.dart';

class BookingActions extends StatelessWidget {
  final Booking booking;

  BookingActions(this.booking);

  @override
  Widget build(BuildContext context) {
    void postUserAction(USER_ACTION_TYPES type) {
      UserActionsProvider.postUserActionFromBooking(
        context,
        booking,
        type,
        VIEWS.bookings,
      );
    }

    return Row(
      children: <Widget>[
        DialogButton(
          size: 35.0,
          imageUrl: 'assets/menu.png',
          dialog: CardContainer(
            child: BookingActionsDialog(
              booking: booking,
              inicialState: STATE.menu,
            ),
          ),
          margin: EdgeInsets.only(
            top: 20.0,
            bottom: 20.0,
          ),
          onTap: () {
            postUserAction(USER_ACTION_TYPES.viewMenu);
          },
        ),
        SizedBox(width: 10.0),
        DialogButton(
          size: 35.0,
          imageUrl: 'assets/edit.png',
          dialog: CardContainer(
            child: BookingActionsDialog(
              booking: booking,
              inicialState: STATE.booking,
            ),
          ),
          margin: EdgeInsets.only(
            top: 20.0,
            bottom: 20.0,
          ),
          onTap: () {
            postUserAction(USER_ACTION_TYPES.viewEditBookingForm);
          },
        ),
      ],
    );
  }
}
