import 'package:flutter/material.dart';
import 'package:reservapp_flutter/src/model/Booking.dart';
import 'package:reservapp_flutter/src/widgets/StyledTitle.dart';
import 'package:reservapp_flutter/src/widgets/bookingWidgets/BookingItem/BookingActions.dart';
import 'package:reservapp_flutter/src/widgets/bookingWidgets/BookingItem/BookingInfoSummary.dart';

class BookingInfo extends StatelessWidget {
  final Booking booking;

  BookingInfo(this.booking);

  @override
  Widget build(BuildContext context) {
    return Expanded(
      child: Column(
        crossAxisAlignment: CrossAxisAlignment.start,
        children: <Widget>[
          StyledTitle(
            text: booking.restaurantName,
            fontSize: 16.0,
          ),
          Row(
            crossAxisAlignment: CrossAxisAlignment.start,
            mainAxisAlignment: MainAxisAlignment.spaceBetween,
            children: <Widget>[
              BookingInfoSummary(booking),
              booking.isPast()
                  ? Column(
                      children: <Widget>[
                        Text(
                          'Booking',
                          style: TextStyle(
                            color: Colors.red[900],
                            fontSize: 15.0,
                            fontWeight: FontWeight.bold,
                          ),
                        ),
                        Text(
                          'Completed',
                          style: TextStyle(
                            color: Colors.red[900],
                            fontSize: 15.0,
                            fontWeight: FontWeight.bold,
                          ),
                        ),
                      ],
                    )
                  : BookingActions(booking),
            ],
          ),
        ],
      ),
    );
  }
}
