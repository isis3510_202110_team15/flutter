import 'package:cloud_firestore/cloud_firestore.dart';
import 'package:firebase_auth/firebase_auth.dart';
import 'package:flutter/material.dart';
import 'package:jiffy/jiffy.dart';
import 'package:reservapp_flutter/routes.dart';
import 'package:reservapp_flutter/src/model/Booking.dart';
import 'package:reservapp_flutter/src/model/Restaurant.dart';
import 'package:reservapp_flutter/src/model/UserAction.dart';
import 'package:reservapp_flutter/src/providers/BookingsProvider.dart';
import 'package:reservapp_flutter/src/providers/UserActionsProvider.dart';
import 'package:reservapp_flutter/src/widgets/StyledTitle.dart';
import 'package:reservapp_flutter/src/widgets/banners/NoConnectivityBanner.dart';
import 'package:reservapp_flutter/src/widgets/bookingWidgets/BookingForm.dart';

class BookingView extends StatefulWidget {
  final Restaurant restaurant;

  BookingView(this.restaurant);

  @override
  _BookingViewState createState() => _BookingViewState(restaurant);
}

enum STATE {
  fillingForm,
  loading,
  done,
  noConnectivity,
}

class _BookingViewState extends State<BookingView> {
  final Restaurant restaurant;
  Booking _booking;
  STATE _state = STATE.fillingForm;
  final userId = FirebaseAuth.instance.currentUser.email;

  _BookingViewState(this.restaurant) {
    DateTime now = DateTime.now();
    final date = Jiffy(now).add(hours: 1);

    _booking = new Booking(
      image: restaurant.image,
      restaurantAddress: restaurant.address,
      restaurantName: restaurant.name,
      restaurantId: restaurant.id,
      restaurantCategory: restaurant.category,
      userId: userId,
      date: date,
      peopleQuantity: 2,
    );
  }

  void postBooking() async {
    if (_state == STATE.fillingForm) {
      setState(() {
        _state = STATE.loading;
      });

      DocumentReference booking = await BookingsProvider.postBooking(
        context,
        _booking,
      );

      if (booking != null) {
        UserActionsProvider.postUserActionFromBooking(
          context,
          _booking,
          USER_ACTION_TYPES.saveBooking,
          VIEWS.bookingsDialog,
        );

        setState(() {
          _state = STATE.done;
        });
      } else {
        setState(() {
          _state = STATE.noConnectivity;
        });
      }
    }
  }

  @override
  Widget build(BuildContext context) {
    if (_state == STATE.noConnectivity) {
      return NoConnectivityBanner(
        text: 'You can book once you have connection!',
      );
    }

    if (_state == STATE.fillingForm) {
      return BookingForm(
        booking: _booking,
        saveBooking: postBooking,
        title: 'Book now!',
      );
    }

    if (_state == STATE.done) {
      return Column(
        children: <Widget>[
          SizedBox(height: 100),
          StyledTitle(
            text: 'We have received your booking!',
            textAlign: TextAlign.center,
          ),
          Container(
            margin: EdgeInsets.all(15.0),
            child: Image.asset('assets/bookings.jpg'),
          ),
          MaterialButton(
            child: Text(
              'Go to bookings',
              style: TextStyle(
                fontSize: 25.0,
                color: Colors.white,
                decoration: TextDecoration.none,
              ),
            ),
            onPressed: () {
              Navigator.pushNamed(
                context,
                ROUTES.bookings.toString(),
              );
            },
            color: Colors.red[700],
          ),
        ],
      );
    }

    return Container(
      height: 400,
      child: Center(
        child: CircularProgressIndicator(),
      ),
    );
  }
}
