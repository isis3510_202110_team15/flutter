import 'package:flutter/material.dart';

class ScrollViewWrapper extends StatelessWidget {
  final List<Widget> children;

  ScrollViewWrapper({
    this.children,
  });

  @override
  Widget build(BuildContext context) {
    return Column(
      children: <Widget>[
        Expanded(
          child: ListView(
            children: children,
          ),
        ),
      ],
    );
  }
}
