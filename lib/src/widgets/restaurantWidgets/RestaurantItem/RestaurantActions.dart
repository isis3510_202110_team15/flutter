import 'package:flutter/material.dart';
import 'package:reservapp_flutter/src/model/Restaurant.dart';
import 'package:reservapp_flutter/src/model/UserAction.dart';
import 'package:reservapp_flutter/src/providers/UserActionsProvider.dart';
import 'package:reservapp_flutter/src/widgets/DialogButton.dart';
import 'package:reservapp_flutter/src/widgets/containers/CardContainer.dart';
import 'package:reservapp_flutter/src/widgets/dialogs/MenuBookingDialog/RestaurantActionsDialog.dart';

class RestaurantActions extends StatelessWidget {
  final Restaurant restaurant;

  RestaurantActions(this.restaurant);

  @override
  Widget build(BuildContext context) {
    void postUserAction(USER_ACTION_TYPES type) {
      UserActionsProvider.postUserActionFromRestaurant(
        context,
        restaurant,
        type,
        VIEWS.restaurants,
      );
    }

    return Row(
      mainAxisAlignment: MainAxisAlignment.spaceBetween,
      children: <Widget>[
        DialogButton(
          size: 35.0,
          imageUrl: 'assets/menu.png',
          dialog: CardContainer(
            child: RestaurantActionsDialog(
              restaurant: restaurant,
              inicialState: STATE.menu,
            ),
          ),
          margin: EdgeInsets.only(
            top: 20.0,
            bottom: 20.0,
          ),
          onTap: () {
            postUserAction(USER_ACTION_TYPES.viewMenu);
          },
        ),
        SizedBox(width: 15.0),
        DialogButton(
          size: 35.0,
          imageUrl: 'assets/calendar.png',
          dialog: CardContainer(
            child: RestaurantActionsDialog(
              restaurant: restaurant,
              inicialState: STATE.booking,
            ),
          ),
          margin: EdgeInsets.only(
            top: 20.0,
            bottom: 20.0,
          ),
          onTap: () {
            postUserAction(USER_ACTION_TYPES.viewBookingForm);
          },
        ),
      ],
    );
  }
}
