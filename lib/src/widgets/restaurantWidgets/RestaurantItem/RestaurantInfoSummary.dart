import 'package:flutter/material.dart';
import 'package:reservapp_flutter/src/model/Restaurant.dart';

class RestaurantInfoSummary extends StatelessWidget {
  final Restaurant restaurant;

  RestaurantInfoSummary(this.restaurant);

  @override
  Widget build(BuildContext context) {
    return Column(
      crossAxisAlignment: CrossAxisAlignment.start,
      children: <Widget>[
        restaurant.kmAway != null
            ? Text(
                restaurant.kmAway > 0.1
                    ? '${restaurant.kmAway.toStringAsFixed(1)} km away'
                    : '${(restaurant.kmAway * 1000).toStringAsFixed(1)} m away',
                style: TextStyle(fontSize: 14),
              )
            : Container(),
        Text(
          restaurant.address,
          style: TextStyle(fontSize: 13),
        ),
        Text(
          restaurant.city,
          style: TextStyle(fontSize: 12),
        ),
        Text(
          restaurant.category,
          style: TextStyle(fontSize: 12),
        ),
      ],
    );
  }
}
