import 'package:flutter/material.dart';
import 'package:reservapp_flutter/src/model/Restaurant.dart';
import 'package:reservapp_flutter/src/widgets/StyledTitle.dart';
import 'package:reservapp_flutter/src/widgets/restaurantWidgets/RestaurantItem/RestaurantActions.dart';
import 'package:reservapp_flutter/src/widgets/restaurantWidgets/RestaurantItem/RestaurantInfoSummary.dart';

class RestaurantInfo extends StatelessWidget {
  final Restaurant restaurant;
  final bool isFavorite;
  RestaurantInfo({
    this.restaurant,
    this.isFavorite,
  });

  @override
  Widget build(BuildContext context) {
    return Expanded(
      child: Column(
        crossAxisAlignment: CrossAxisAlignment.start,
        children: <Widget>[
          isFavorite
              ? Row(
                  crossAxisAlignment: CrossAxisAlignment.center,
                  children: <Widget>[
                    Icon(
                      Icons.star,
                    ),
                    SizedBox(width: 5.0),
                    Text(
                      'Recommended for you',
                      style: TextStyle(
                        fontSize: 12,
                        fontWeight: FontWeight.bold,
                      ),
                    ),
                  ],
                )
              : Container(),
          StyledTitle(
            text: restaurant.name,
            fontSize: 16.0,
          ),
          Row(
            crossAxisAlignment: CrossAxisAlignment.start,
            mainAxisAlignment: MainAxisAlignment.spaceBetween,
            children: <Widget>[
              RestaurantInfoSummary(restaurant),
              RestaurantActions(restaurant),
            ],
          ),
        ],
      ),
    );
  }
}
