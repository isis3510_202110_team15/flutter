import 'package:flutter/material.dart';
import 'package:reservapp_flutter/src/model/Restaurant.dart';
import 'package:reservapp_flutter/src/widgets/ItemImage.dart';
import 'package:reservapp_flutter/src/widgets/restaurantWidgets/RestaurantItem/RestaurantInfo.dart';

class RestaurantItem extends StatelessWidget {
  final Restaurant restaurant;

  RestaurantItem({
    this.restaurant,
  });

  @override
  Widget build(BuildContext context) {
    return Card(
      elevation: 10,
      margin: const EdgeInsets.only(
        bottom: 20.0,
      ),
      child: Padding(
        padding: const EdgeInsets.symmetric(vertical: 10, horizontal: 8),
        child: Row(
          mainAxisAlignment: MainAxisAlignment.spaceBetween,
          children: <Widget>[
            ItemImage(
              imageUrl: restaurant.image,
            ),
            RestaurantInfo(
              restaurant: restaurant,
              isFavorite: restaurant.isFavorite(),
            ),
          ],
        ),
      ),
    );
  }
}
