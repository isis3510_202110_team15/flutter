import 'package:flutter/material.dart';
import 'package:reservapp_flutter/src/model/Restaurant.dart';
import 'package:reservapp_flutter/src/widgets/banners/NoConnectivityBanner.dart';
import 'RestaurantItem/RestaurantItem.dart';

class RestaurantList extends StatefulWidget {
  final List<Restaurant> restaurants;

  RestaurantList(this.restaurants);

  @override
  _RestaurantListState createState() => _RestaurantListState(restaurants);
}

class _RestaurantListState extends State<RestaurantList> {
  final List<Restaurant> restaurants;

  _RestaurantListState(this.restaurants);

  @override
  Widget build(BuildContext context) {
    if (restaurants == null) {
      return NoConnectivityBanner();
    }

    return Container(
      padding: EdgeInsets.all(10),
      child: restaurants.length > 0
          ? ListView.builder(
              itemCount: restaurants.length,
              itemBuilder: (BuildContext context, int index) {
                Restaurant restaurant = restaurants[index];
                return RestaurantItem(
                  restaurant: restaurant,
                );
              },
            )
          : Text(
              'We couldn´t find any restaurants, please try later :)',
              style: TextStyle(
                fontSize: 25.0,
                color: Colors.grey[900],
                decoration: TextDecoration.none,
              ),
            ),
    );
  }
}
