import 'package:flutter/material.dart';

class DialogButton extends StatefulWidget {
  final Widget dialog;
  final String imageUrl;
  final double size;
  final EdgeInsetsGeometry margin;
  final Function onTap;

  DialogButton({
    this.dialog,
    this.imageUrl,
    this.size,
    this.margin,
    this.onTap,
  });

  @override
  _DialogButtonState createState() => _DialogButtonState(
        dialog: dialog,
        imageUrl: imageUrl,
        size: size,
        margin: margin,
        onTap: onTap,
      );
}

class _DialogButtonState extends State<DialogButton> {
  final Widget dialog;
  final String imageUrl;
  final double size;
  final EdgeInsetsGeometry margin;
  final Function onTap;

  _DialogButtonState({
    this.dialog,
    this.imageUrl,
    this.size,
    this.margin,
    this.onTap,
  });

  @override
  Widget build(BuildContext context) {
    return GestureDetector(
      onTap: () {
        onTap();
        showGeneralDialog(
          context: context,
          barrierDismissible: true,
          barrierLabel:
              MaterialLocalizations.of(context).modalBarrierDismissLabel,
          transitionDuration: Duration(milliseconds: 500),
          pageBuilder:
              (BuildContext context, Animation first, Animation second) {
            return Container(
              child: dialog,
            );
          },
        );
      },
      child: Container(
        margin: margin,
        height: size,
        width: size,
        child: Image(
          image: AssetImage(imageUrl),
          width: size,
          height: size,
        ),
      ),
    );
  }
}
