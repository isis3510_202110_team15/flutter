import 'package:cached_network_image/cached_network_image.dart';
import 'package:cloud_firestore/cloud_firestore.dart';
import 'package:firebase_auth/firebase_auth.dart';
import 'package:flutter/material.dart';
import 'package:reservapp_flutter/routes.dart';
import 'package:reservapp_flutter/src/pages/RestaurantsView.dart';
import 'package:reservapp_flutter/src/pages/ProfileView.dart';
import 'package:reservapp_flutter/src/widgets/containers/ViewContainer.dart';

class MenuDrawer extends StatefulWidget {
  _MenuDrawerState createState() => _MenuDrawerState();
}

class _MenuDrawerState extends State<MenuDrawer> {
  final _auth = FirebaseAuth.instance;

  Widget nombre(BuildContext context) {
    CollectionReference users = FirebaseFirestore.instance.collection('users');

    return FutureBuilder<DocumentSnapshot>(
      future: users.doc(_auth.currentUser.email).get(),
      builder:
          (BuildContext context, AsyncSnapshot<DocumentSnapshot> snapshot) {
        if (snapshot.hasError) {
          return Text("Something went wrong");
        }

        if (snapshot.connectionState == ConnectionState.done) {
          Map<String, dynamic> data = snapshot.data.data();
          return Text("${data['fullName']} ");
        }

        return Text("loading");
      },
    );
  }

  @override
  Widget build(
    BuildContext context,
  ) {
    return Drawer(
      child: ListView(
        children: <Widget>[
          new UserAccountsDrawerHeader(
            accountName: nombre(context),
            accountEmail: new Text(_auth.currentUser.email),
            currentAccountPicture: ClipRRect(
              borderRadius: BorderRadius.circular(10000.0),
              child: CachedNetworkImage(
                placeholder: (context, url) => Container(
                  margin: EdgeInsets.all(20.0),
                  child: CircularProgressIndicator(),
                ),
                imageUrl: 'https://i.pravatar.cc/150?img=2',
              ),
            ),
          ),
          new ListTile(
            title: new Text('My Profile'),
            onTap: () {
              Navigator.of(context).pop();
              Navigator.push(
                context,
                new MaterialPageRoute(
                  builder: (BuildContext context) => new ProfileView(),
                ),
              );
            },
          ),
          new Divider(
            color: Colors.black,
          ),
          new ListTile(
            title: new Text('Bookings'),
            onTap: () {
              Navigator.pushNamed(
                context,
                ROUTES.bookings.toString(),
              );
            },
          ),
          new Divider(
            color: Colors.black,
          ),
          new ListTile(
            title: new Text('Home'),
            onTap: () {
              Navigator.pushNamed(
                context,
                ROUTES.home.toString(),
              );
            },
          ),
          new Divider(
            color: Colors.black,
          ),
          new ListTile(
            title: new Text('Restaurants'),
            onTap: () {
              Navigator.of(context).pop();
              Navigator.push(
                context,
                new MaterialPageRoute(
                  builder: (BuildContext context) => ViewContainer(
                    title: 'Restaurants',
                    child: RestaurantsView(),
                  ),
                ),
              );
            },
          ),
          new Divider(
            color: Colors.black,
          ),
          new ListTile(
              title: new Text('Analytics'),
              onTap: () {
                Navigator.pushNamed(
                  context,
                  ROUTES.analytics.toString(),
                );
              }),
          new Divider(
            color: Colors.black,
          ),
          new ListTile(
            title: new Text('Sign Out'),
            onTap: () async {
              await signOut();
              Navigator.pushNamed(context, ROUTES.login.toString());
            },
          ),
        ],
      ),
    );
  }

  Future signOut() async {
    try {
      return await _auth.signOut();
    } catch (e) {
      return null;
    }
  }
}
