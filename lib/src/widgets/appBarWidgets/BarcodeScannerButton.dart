import 'package:flutter/material.dart';
import 'package:barcode_scan/barcode_scan.dart';
import 'package:reservapp_flutter/src/model/Restaurant.dart';
import 'package:reservapp_flutter/src/model/UserAction.dart';
import 'package:reservapp_flutter/src/providers/RestaurantsProvider.dart';
import 'package:reservapp_flutter/src/providers/UserActionsProvider.dart';
import 'package:reservapp_flutter/src/utils/showGeneralCardDialog.dart';
import 'package:reservapp_flutter/src/widgets/dialogs/MenuBookingDialog/RestaurantActionsDialog.dart';
import 'package:top_snackbar_flutter/custom_snack_bar.dart';
import 'package:top_snackbar_flutter/top_snack_bar.dart';

class BarcodeScannerButton extends StatefulWidget {
  @override
  _BarcodeScannerButtonState createState() => _BarcodeScannerButtonState();
}

class _BarcodeScannerButtonState extends State<BarcodeScannerButton> {
  void onScan() async {
    String qrCode = await BarcodeScanner.scan();
    try {
      Restaurant restaurant = await RestaurantsProvider.getRestaurantByQrCode(
        context,
        qrCode,
      );

      if (restaurant != null) {
        UserActionsProvider.postUserActionFromRestaurant(
          context,
          restaurant,
          USER_ACTION_TYPES.viewMenu,
          VIEWS.qrCode,
        );

        showGeneralCardDialog(
          context: context,
          child: RestaurantActionsDialog(
            restaurant: restaurant,
            inicialState: STATE.menu,
          ),
        );
      }
    } catch (error) {
      showTopSnackBar(
        context,
        CustomSnackBar.error(
          message: 'Invalid code',
        ),
      );
    }
  }

  @override
  Widget build(
    BuildContext context,
  ) {
    return Padding(
      padding: EdgeInsets.only(right: 20.0),
      child: GestureDetector(
        onTap: onScan,
        child: Icon(
          Icons.qr_code,
        ),
      ),
    );
  }
}
