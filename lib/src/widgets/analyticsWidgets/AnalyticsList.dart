import 'package:flutter/cupertino.dart';
import 'package:flutter/material.dart';
import 'package:line_awesome_flutter/line_awesome_flutter.dart';
import 'package:reservapp_flutter/src/model/Info.dart';
import 'package:reservapp_flutter/src/providers/InfoProvider.dart';

class AnalyticsList extends StatefulWidget {
  @override
  _AnalyticsListState createState() => _AnalyticsListState();
}

class _AnalyticsListState extends State<AnalyticsList> {
  InfoProvider a = new InfoProvider();
  Info m = new Info();
  bool xd;
  @override
  Widget build(BuildContext context) {
    return FutureBuilder(
      future: a.getQuantities(context),
      builder: (context, snapshot) {
        m = snapshot.data;
        if (a.hasConnection == false) {
          return Column(
            children: <Widget>[
              SizedBox(
                height: 30.0 * 5,
              ),
              Container(
                margin: EdgeInsets.all(15.0),
                child: Column(
                  mainAxisAlignment: MainAxisAlignment.center,
                  children: <Widget>[
                    Image(
                      image: AssetImage('assets/no_connectivity.png'),
                    ),
                    Text(
                      "No connection available",
                      textAlign: TextAlign.center,
                      style: TextStyle(
                        fontSize: 30.0,
                        fontWeight: FontWeight.bold,
                      ),
                    ),
                  ],
                ),
              )
            ],
          );
        }
        if (a.isNew == true) {
          return Column(
            children: <Widget>[
              Row(
                children: [
                  SizedBox(
                    height: 120.0 * 5,
                  ),
                  SizedBox(
                    width: 8.0 * 5,
                  ),
                  Text(
                    "There is no data available for you, \nconsider making some bookings.",
                    style: TextStyle(
                      fontSize: 20,
                      fontWeight: FontWeight.w600,
                    ),
                    textAlign: TextAlign.center,
                  )
                ],
              ),
            ],
          );
        }

        if (snapshot.hasData) {
          return Column(
            children: <Widget>[
              Container(
                child: Row(
                  children: [
                    Icon(
                      LineAwesomeIcons.user,
                      size: 15.0 * 3,
                    ),
                    SizedBox(
                      width: 5.0 * 4,
                    ),
                    Column(
                      children: [
                        Text(
                          "We noticed you are a:",
                          style: TextStyle(
                              fontStyle: FontStyle.italic,
                              fontWeight: FontWeight.w600,
                              fontSize: 20),
                        ),
                        Text(m.userCategory,
                            textAlign: TextAlign.center,
                            style: TextStyle(
                              fontSize: 20,
                            ))
                      ],
                    ),
                    Flexible(
                      flex: 1,
                      child: SizedBox(
                        width: 20.0 * 4,
                      ),
                    ),
                    IconButton(
                      icon: Icon(LineAwesomeIcons.question),
                      onPressed: () {
                        showDialog(
                          context: context,
                          builder: (context) => AlertDialog(
                            title: Text("User Category"),
                            content: Text(
                                "LoneWolf: You usually make reservations just for 1.\nLover: You usually make reservations for 2.\nSocialite: You usually maker reservations for more than 2"),
                          ),
                        );
                      },
                    )
                  ],
                ),
              ),
              new Divider(
                color: Colors.black,
                thickness: 2,
              ),
              Container(
                child: Row(
                  children: [
                    Icon(
                      LineAwesomeIcons.star,
                      size: 15.0 * 3,
                    ),
                    SizedBox(
                      width: 5.0 * 4,
                    ),
                    Column(
                      children: [
                        Text(
                          "Your favourite category is:",
                          style: TextStyle(
                              fontStyle: FontStyle.italic,
                              fontWeight: FontWeight.w600,
                              fontSize: 20),
                        ),
                        Text(
                          m.favCat,
                          textAlign: TextAlign.center,
                          style: TextStyle(
                            fontSize: 20,
                          ),
                        ),
                      ],
                    )
                  ],
                ),
              ),
              new Divider(
                color: Colors.black,
                thickness: 2,
              ),
              Container(
                child: Row(
                  children: [
                    Icon(
                      LineAwesomeIcons.jedi_order,
                      size: 15.0 * 3,
                    ),
                    SizedBox(
                      width: 5.0 * 4,
                    ),
                    Column(
                      children: [
                        Text("Your favourite restaurant is:",
                            style: TextStyle(
                                fontStyle: FontStyle.italic,
                                fontWeight: FontWeight.w600,
                                fontSize: 20)),
                        Text(
                          m.favRest,
                          textAlign: TextAlign.center,
                          style: TextStyle(
                            fontSize: 20,
                          ),
                        ),
                      ],
                    ),
                  ],
                ),
              ),
              new Divider(
                color: Colors.black,
                thickness: 2,
              ),
              Container(
                child: Row(
                  children: [
                    Icon(
                      LineAwesomeIcons.hourglass,
                      size: 15.0 * 3,
                    ),
                    SizedBox(
                      width: 5.0 * 4,
                    ),
                    Column(
                      children: [
                        Text(
                          "Your favourite time of the day \nto make a reservation is:",
                          style: TextStyle(
                              fontStyle: FontStyle.italic,
                              fontWeight: FontWeight.w600,
                              fontSize: 20),
                        ),
                        Text(
                          m.favTime,
                          textAlign: TextAlign.center,
                          style: TextStyle(
                            fontSize: 20,
                          ),
                        ),
                      ],
                    )
                  ],
                ),
              ),
              new Divider(
                color: Colors.black,
                thickness: 2,
              ),
              Container(
                child: Row(
                  children: [
                    Icon(
                      LineAwesomeIcons.street_view,
                      size: 15.0 * 3,
                    ),
                    SizedBox(
                      width: 5.0 * 4,
                    ),
                    Column(
                      children: [
                        Text("Your favourite neighborhood \nto eat is:",
                            style: TextStyle(
                                fontStyle: FontStyle.italic,
                                fontWeight: FontWeight.w600,
                                fontSize: 20)),
                        Text(
                          m.favNeigh,
                          textAlign: TextAlign.center,
                          style: TextStyle(
                            fontSize: 20,
                          ),
                        ),
                      ],
                    )
                  ],
                ),
              )
            ],
          );
        } else {
          return Center(child: CircularProgressIndicator());
        }
      },
    );
  }
}
