import 'package:cached_network_image/cached_network_image.dart';
import 'package:cloud_firestore/cloud_firestore.dart';
import 'package:flutter/material.dart';
import 'package:reservapp_flutter/src/model/Category.dart';
import 'package:reservapp_flutter/src/model/UserAction.dart';
import 'package:reservapp_flutter/src/pages/RestaurantsView.dart';
import 'package:reservapp_flutter/src/providers/NotificationsProvider.dart';
import 'package:reservapp_flutter/src/providers/UserActionsProvider.dart';
import 'package:reservapp_flutter/src/utils/checkConnectivity.dart';
import 'package:reservapp_flutter/src/widgets/containers/ViewContainer.dart';

class CategoryList extends StatefulWidget {
  _CategoryListState createState() => _CategoryListState();
}

class _CategoryListState extends State<CategoryList> {
  List<Category> categories;

  Future<QuerySnapshot> getData() {
    checkConnectivity(context);
    NotificationsProvider.handleCategoryNotification(context);
    return FirebaseFirestore.instance.collection('categories').get();
  }

  void onPressedCategory(String category) {
    final userAction = UserAction(
      restaurantCategory: category,
      type: USER_ACTION_TYPES.viewCategory,
      view: VIEWS.home,
    );
    UserActionsProvider.postUserAction(
      context,
      userAction,
    );
    Navigator.push(
      context,
      new MaterialPageRoute(
        builder: (BuildContext context) => ViewContainer(
          title: '$category Restaurants',
          child: RestaurantsView(
            category: category,
          ),
        ),
      ),
    );
  }

  @override
  Widget build(BuildContext context) {
    var color = 0xffffD740;
    return FutureBuilder(
      future: getData(),
      builder: (context, snapshot) {
        if (snapshot.hasData) {
          List<dynamic> categories = snapshot.data.docs;
          return GridView.builder(
            gridDelegate: SliverGridDelegateWithFixedCrossAxisCount(
              mainAxisExtent: 150,
              crossAxisCount: 3,
              crossAxisSpacing: 20,
              mainAxisSpacing: 20,
            ),
            itemCount: categories.length,
            itemBuilder: (context, i) {
              String categoryName = categories[i].data()['name'];
              return GestureDetector(
                onTap: () => onPressedCategory(categoryName),
                child: Container(
                  decoration: BoxDecoration(
                      color: Color(color),
                      borderRadius: BorderRadius.circular(10)),
                  child: Column(
                    mainAxisAlignment: MainAxisAlignment.center,
                    children: <Widget>[
                      CachedNetworkImage(
                        placeholder: (context, url) =>
                            CircularProgressIndicator(),
                        imageUrl: categories[i].data()['image'],
                      ),
                      SizedBox(
                        height: 14,
                      ),
                      Text(categoryName),
                    ],
                  ),
                ),
              );
            },
          );
        } else {
          return Center(child: CircularProgressIndicator());
        }
      },
    );
  }
}
