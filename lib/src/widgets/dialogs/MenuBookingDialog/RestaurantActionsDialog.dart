import 'package:flutter/material.dart';
import 'package:reservapp_flutter/src/model/Restaurant.dart';
import 'package:reservapp_flutter/src/model/UserAction.dart';
import 'package:reservapp_flutter/src/providers/UserActionsProvider.dart';
import 'package:reservapp_flutter/src/widgets/StyledTitle.dart';
import 'package:reservapp_flutter/src/widgets/bookingWidgets/BookingView.dart';
import 'package:reservapp_flutter/src/widgets/dialogs/MenuBookingDialog/DialogSelector.dart';
import 'package:reservapp_flutter/src/widgets/menuWidgets/MenuBody/MenuBody.dart';

enum STATE {
  booking,
  menu,
}

final stateTranslations = {
  STATE.booking: 'Book',
  STATE.menu: 'Menu',
};

class RestaurantActionsDialog extends StatefulWidget {
  final Restaurant restaurant;
  final STATE inicialState;

  RestaurantActionsDialog({
    this.restaurant,
    this.inicialState: STATE.menu,
  });

  @override
  _RestaurantActionsDialogState createState() => _RestaurantActionsDialogState(
        restaurant: restaurant,
        inicialState: inicialState,
      );
}

class _RestaurantActionsDialogState extends State<RestaurantActionsDialog> {
  final Restaurant restaurant;
  final STATE inicialState;
  STATE _state;

  _RestaurantActionsDialogState({
    this.restaurant,
    this.inicialState: STATE.menu,
  }) {
    _state = inicialState;
  }

  void _setState(STATE newState) {
    setState(() {
      _state = newState;
    });
  }

  void postUserAction(USER_ACTION_TYPES type) {
    UserActionsProvider.postUserActionFromRestaurant(
      context,
      restaurant,
      type,
      VIEWS.restaurantsDialog,
    );
  }

  void onHorizontalDrag(details) {
    // Note: Sensitivity is integer used when you don't want to mess up vertical drag
    int sensitivity = 1;
    if (details.delta.dx > sensitivity && _state != STATE.menu) {
      _setState(STATE.menu);
      postUserAction(USER_ACTION_TYPES.viewMenu);
    } else if (details.delta.dx < -sensitivity && _state != STATE.booking) {
      _setState(STATE.booking);
      postUserAction(USER_ACTION_TYPES.viewBookingForm);
    }
  }

  @override
  Widget build(BuildContext context) {
    return Column(
      children: <Widget>[
        SizedBox(
          height: 10,
        ),
        StyledTitle(
          text: restaurant.name,
          textAlign: TextAlign.center,
        ),
        SizedBox(height: 20),
        Row(
          mainAxisAlignment: MainAxisAlignment.spaceAround,
          children: <Widget>[
            DialogSelector(
              label: stateTranslations[STATE.menu],
              isSelected: _state == STATE.menu,
              onTap: () {
                _setState(STATE.menu);
                postUserAction(USER_ACTION_TYPES.viewMenu);
              },
            ),
            DialogSelector(
              label: stateTranslations[STATE.booking],
              isSelected: _state == STATE.booking,
              onTap: () {
                _setState(STATE.booking);
                postUserAction(USER_ACTION_TYPES.viewBookingForm);
              },
            ),
          ],
        ),
        Expanded(
          child: GestureDetector(
            child: _state == STATE.menu
                ? MenuBody(restaurant.id)
                : BookingView(restaurant),
            onHorizontalDragUpdate: onHorizontalDrag,
          ),
        ),
      ],
    );
  }
}
