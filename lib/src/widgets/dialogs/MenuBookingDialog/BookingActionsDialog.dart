import 'package:flutter/material.dart';
import 'package:reservapp_flutter/src/model/Booking.dart';
import 'package:reservapp_flutter/src/model/UserAction.dart';
import 'package:reservapp_flutter/src/providers/UserActionsProvider.dart';
import 'package:reservapp_flutter/src/widgets/StyledTitle.dart';
import 'package:reservapp_flutter/src/widgets/bookingWidgets/EditBookingView.dart';
import 'package:reservapp_flutter/src/widgets/dialogs/MenuBookingDialog/DialogSelector.dart';
import 'package:reservapp_flutter/src/widgets/menuWidgets/MenuBody/MenuBody.dart';

enum STATE {
  booking,
  menu,
}

final stateTranslations = {
  STATE.booking: 'Edit Booking',
  STATE.menu: 'Menu',
};

class BookingActionsDialog extends StatefulWidget {
  final Booking booking;
  final STATE inicialState;

  BookingActionsDialog({
    this.booking,
    this.inicialState: STATE.menu,
  });

  @override
  _BookingActionsDialogState createState() => _BookingActionsDialogState(
        booking: booking,
        inicialState: inicialState,
      );
}

class _BookingActionsDialogState extends State<BookingActionsDialog> {
  final Booking booking;
  final STATE inicialState;
  STATE _state;

  _BookingActionsDialogState({
    this.booking,
    this.inicialState: STATE.menu,
  }) {
    _state = inicialState;
  }

  void _setState(STATE newState) {
    setState(() {
      _state = newState;
    });
  }

  void postUserAction(USER_ACTION_TYPES type) {
    UserActionsProvider.postUserActionFromBooking(
      context,
      booking,
      type,
      VIEWS.bookingsDialog,
    );
  }

  void onHorizontalDrag(details) {
    // Note: Sensitivity is integer used when you don't want to mess up vertical drag
    int sensitivity = 1;
    if (details.delta.dx > sensitivity && _state != STATE.menu) {
      _setState(STATE.menu);
      postUserAction(USER_ACTION_TYPES.viewMenu);
    } else if (details.delta.dx < -sensitivity && _state != STATE.booking) {
      _setState(STATE.booking);
      postUserAction(USER_ACTION_TYPES.viewEditBookingForm);
    }
  }

  @override
  Widget build(BuildContext context) {
    return Column(
      children: <Widget>[
        SizedBox(height: 10),
        StyledTitle(
          text: booking.restaurantName,
          textAlign: TextAlign.center,
        ),
        SizedBox(height: 20),
        Row(
          mainAxisAlignment: MainAxisAlignment.spaceAround,
          children: <Widget>[
            DialogSelector(
              label: stateTranslations[STATE.menu],
              isSelected: _state == STATE.menu,
              onTap: () {
                _setState(STATE.menu);
                postUserAction(USER_ACTION_TYPES.viewMenu);
              },
            ),
            DialogSelector(
              label: stateTranslations[STATE.booking],
              isSelected: _state == STATE.booking,
              onTap: () {
                _setState(STATE.booking);
                postUserAction(USER_ACTION_TYPES.viewEditBookingForm);
              },
            ),
          ],
        ),
        Expanded(
          child: GestureDetector(
            child: _state == STATE.menu
                ? MenuBody(booking.restaurantId)
                : EditBookingView(booking),
            onHorizontalDragUpdate: onHorizontalDrag,
          ),
        ),
      ],
    );
  }
}
