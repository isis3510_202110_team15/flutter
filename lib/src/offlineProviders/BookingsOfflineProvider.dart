import 'package:reservapp_flutter/src/model/Booking.dart';
import 'package:reservapp_flutter/src/model/UserActionStatistics.dart';
import 'package:reservapp_flutter/src/offlineProviders/DatabaseProvider.dart';
import 'package:sqflite/sqflite.dart';

class BookingsOfflineProvider {
  static void createBookingsTable(Database db, int version) {
    db.execute(
      ''' 
      CREATE TABLE $bookingTableName( 
      $dbId $idType,
      $idKey $textType ,
      $imageKey $textType,
      $restaurantAddressKey $textType, 
      $restaurantNameKey $textType, 
      $restaurantIdKey $textType, 
      $restaurantCategoryKey $textType,
      $userIdKey $textType, 
      $dateKey $textType,
      $peopleQuantityKey $integerType
      )
      ''',
    );
  }

  static Future<int> insert(Booking booking) async {
    final bookingRow = booking.toSQLRow();
    final id = await DatabaseProvider.instance.insert(
      tableName: bookingTableName,
      row: bookingRow,
    );

    return id;
  }

  static Future<List<Booking>> getBookings(String userId) async {
    List<Map<String, Object>> bookingRows =
        await DatabaseProvider.instance.query(
      tableName: bookingTableName,
      stringQuery: '$userIdKey = "$userId"',
    );

    List<Booking> bookings = Booking.makeBookingListFromSQLRows(
      bookingRows,
    );

    bookings = bookings.length == 0 ? null : bookings;

    return bookings;
  }
}
