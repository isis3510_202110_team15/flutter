import 'package:reservapp_flutter/src/model/UserActionStatistics.dart';
import 'package:reservapp_flutter/src/offlineProviders/SharedPreferencesProvider.dart';
import 'package:reservapp_flutter/src/providers/UserActionStadisticsProvider.dart';
import 'package:reservapp_flutter/src/utils/checkConnectivity.dart';

final String userActionStadisticsCalculatedKey =
    'userActionStadisticsCalculated';

class UserActionStadisticsOfflineProvider {
  static Future<UserActionStatistics> getUserActionStadistics(context) async {
    UserActionStatistics userActionStatistics;
    bool userActionStadisticsCalculated =
        await SharedPreferencesProvider.getBool(
            userActionStadisticsCalculatedKey);

    if (userActionStadisticsCalculated == true) {
      final favoriteRestaurantId = await SharedPreferencesProvider.getString(
        favoriteRestaurantIdKey,
      );
      final favoriteRestaurantName = await SharedPreferencesProvider.getString(
        favoriteRestaurantNameKey,
      );
      final favoriteRestaurantAddress =
          await SharedPreferencesProvider.getString(
        favoriteRestaurantAddressKey,
      );
      final favoriteRestaurantCategory =
          await SharedPreferencesProvider.getString(
        favoriteRestaurantCategoryKey,
      );
      final favoriteRestaurantIdCount = await SharedPreferencesProvider.getInt(
        favoriteRestaurantIdCountKey,
      );
      final favoriteRestaurantNameCount =
          await SharedPreferencesProvider.getInt(
        favoriteRestaurantNameCountKey,
      );
      final favoriteRestaurantAddressCount =
          await SharedPreferencesProvider.getInt(
        favoriteRestaurantAddressCountKey,
      );
      final favoriteRestaurantCategoryCount =
          await SharedPreferencesProvider.getInt(
        favoriteRestaurantCategoryCountKey,
      );
      final restaurantIdStatistics =
          await SharedPreferencesProvider.getIntMapFromStingList(
        restaurantIdStatisticsKey,
      );
      final restaurantNameStatistics =
          await SharedPreferencesProvider.getIntMapFromStingList(
        restaurantNameStatisticsKey,
      );
      final restaurantAddressStatistics =
          await SharedPreferencesProvider.getIntMapFromStingList(
        restaurantAddressStatisticsKey,
      );
      final restaurantCategoryStatistics =
          await SharedPreferencesProvider.getIntMapFromStingList(
        restaurantCategoryStatisticsKey,
      );

      userActionStatistics = UserActionStatistics(
        favoriteRestaurantId: favoriteRestaurantId,
        favoriteRestaurantName: favoriteRestaurantName,
        favoriteRestaurantAddress: favoriteRestaurantAddress,
        favoriteRestaurantCategory: favoriteRestaurantCategory,
        favoriteRestaurantIdCount: favoriteRestaurantIdCount,
        favoriteRestaurantNameCount: favoriteRestaurantNameCount,
        favoriteRestaurantAddressCount: favoriteRestaurantAddressCount,
        favoriteRestaurantCategoryCount: favoriteRestaurantCategoryCount,
        restaurantIdStatistics: restaurantIdStatistics,
        restaurantNameStatistics: restaurantNameStatistics,
        restaurantAddressStatistics: restaurantAddressStatistics,
        restaurantCategoryStatistics: restaurantCategoryStatistics,
      );
    } else if (await checkConnectivity(context)) {
      userActionStatistics =
          await UserActionStadisticsProvider.calculateUserActionStatistics(
              context);
    }
    return userActionStatistics;
  }

  static Future<void> setUserActionStadistics(
    UserActionStatistics userActionStatistics,
  ) async {
    await SharedPreferencesProvider.setMap(
      userActionStatistics.toSharedPreferencesMap(),
    );
    await SharedPreferencesProvider.setBool(
        userActionStadisticsCalculatedKey, true);
  }
}
