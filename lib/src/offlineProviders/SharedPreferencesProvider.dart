import 'package:shared_preferences/shared_preferences.dart';

class SharedPreferencesProvider {
  static Future<bool> getBool(String key) async {
    final SharedPreferences prefs = await SharedPreferences.getInstance();

    return prefs.getBool(key) ?? null;
  }

  static Future<bool> setBool(
    String key,
    bool boolValue,
  ) async {
    final SharedPreferences prefs = await SharedPreferences.getInstance();

    return prefs.setBool(key, boolValue);
  }

  static Future<int> getInt(String key) async {
    final SharedPreferences prefs = await SharedPreferences.getInstance();

    return prefs.getInt(key) ?? null;
  }

  static Future<bool> setInt(
    String key,
    int intValue,
  ) async {
    final SharedPreferences prefs = await SharedPreferences.getInstance();

    return prefs.setInt(key, intValue);
  }

  static Future<String> getString(String key) async {
    final SharedPreferences prefs = await SharedPreferences.getInstance();

    return prefs.getString(key) ?? null;
  }

  static Future<bool> setString(
    String key,
    String stringValue,
  ) async {
    final SharedPreferences prefs = await SharedPreferences.getInstance();

    return prefs.setString(key, stringValue);
  }

  static Future<List<String>> getStringList(String key) async {
    final SharedPreferences prefs = await SharedPreferences.getInstance();

    return prefs.getStringList(key) ?? null;
  }

  static Future<bool> setStringList(
    String key,
    List<String> stringList,
  ) async {
    final SharedPreferences prefs = await SharedPreferences.getInstance();

    return prefs.setStringList(key, stringList);
  }

  static List<String> mapToStringList(
    Map<String, dynamic> map,
  ) {
    final List<String> stringList = [];
    map.map((key, value) {
      stringList.add(key);
      stringList.add(value.toString());
      return MapEntry(key, value);
    });

    return stringList;
  }

  static Future<Map<String, int>> getIntMapFromStingList(
    String key,
  ) async {
    List<String> stringList = await SharedPreferencesProvider.getStringList(
      key,
    );
    final Map<String, int> map = {};
    if (stringList != null) {
      for (int i = 0; i < stringList.length - 1; i += 2) {
        String key = stringList[i];
        int value = int.parse(stringList[i + 1]);
        map[key] = value;
      }
    }

    return map;
  }

  static Future<void> setMap(
    Map<String, dynamic> map,
  ) async {
    map.map((key, value) {
      final valueType = value.runtimeType;

      if (valueType == bool) {
        setBool(key, value);
      } else if (valueType == int) {
        setInt(key, value);
      } else if (valueType == String) {
        setString(key, value);
      } else if (valueType.toString() == 'List<String>') {
        setStringList(key, value);
      } else {
        throw Error();
      }

      return MapEntry(key, value);
    });
  }
}
