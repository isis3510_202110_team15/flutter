import 'package:reservapp_flutter/src/offlineProviders/BookingsOfflineProvider.dart';
import 'package:sqflite/sqflite.dart';
import 'package:path/path.dart';

final String dbId = '_id';
final String idKey = 'id';

final String idType = 'INTEGER PRIMARY KEY AUTOINCREMENT';
final String textType = 'TEXT NOT NULL';
final String integerType = 'INTEGER NOT NULL';

class DatabaseProvider {
  static final _dbName = 'reservapp.db';
  static final _dbVersion = 1;

  static Database _database;
  static final DatabaseProvider instance = DatabaseProvider._init();
  DatabaseProvider._init();

  Future<Database> get database async {
    if (_database != null) {
      return _database;
    }
    _database = await _inititeDatabase(_dbName);

    return _database;
  }

  _inititeDatabase(String filePath) async {
    String dbPath = await getDatabasesPath();
    final path = join(dbPath, filePath);
    final db = await openDatabase(
      path,
      version: _dbVersion,
      onCreate: _onCreate,
    );
    return db;
  }

  void _onCreate(Database db, int version) {
    BookingsOfflineProvider.createBookingsTable(db, version);
  }

  Future<int> insert({
    String tableName,
    Map<String, Object> row,
  }) async {
    Database db = await instance.database;
    final id = await db.insert(tableName, row);
    return id;
  }

  Future<List<Map<String, Object>>> query({
    String tableName,
    String stringQuery,
  }) async {
    Database db = await instance.database;
    List<Map<String, Object>> rows = await db.query(
      tableName,
      where: stringQuery,
    );
    return rows;
  }

  Future<int> update({
    String tableName,
    Map<String, Object> row,
    String stringQuery: '',
  }) async {
    Database db = await instance.database;
    final updatedCount = await db.update(
      tableName,
      row,
      where: stringQuery,
    );
    return updatedCount;
  }

  Future<int> delete({
    String tableName,
    String stringQuery: '',
  }) async {
    Database db = await instance.database;
    final deletedCount = await db.delete(
      tableName,
      where: stringQuery,
    );
    return deletedCount;
  }

  Future close() async {
    final db = await instance.database;
    db.close();
  }
}
