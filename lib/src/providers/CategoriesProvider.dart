import 'package:cloud_firestore/cloud_firestore.dart';

class CategoriesProvider {
  final CollectionReference categoriesList =
      FirebaseFirestore.instance.collection('categories');
}
