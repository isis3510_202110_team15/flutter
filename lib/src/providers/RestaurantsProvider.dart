import 'package:cloud_firestore/cloud_firestore.dart';
import 'package:flutter/cupertino.dart';
import 'package:location/location.dart';
import 'package:reservapp_flutter/src/model/Restaurant.dart';
import 'package:reservapp_flutter/src/offlineProviders/UserActionStadisticsOfflineProvider.dart';
import 'package:reservapp_flutter/src/providers/RestaurantsRecycler.dart';
import 'package:reservapp_flutter/src/utils/LocationsService.dart';
import 'package:reservapp_flutter/src/utils/checkConnectivity.dart';

class RestaurantsProvider {
  static Future<RestaurantsRecycler> getRestaurantsRecycler(
    BuildContext context,
    String category,
  ) async {
    if (await checkConnectivity(context)) {
      final CollectionReference restaurantsCollection =
          FirebaseFirestore.instance.collection(restaurantsTableName);

      QuerySnapshot restaurantsQuerySnapshot;

      if (category == '') {
        restaurantsQuerySnapshot = await restaurantsCollection.get();
      } else {
        restaurantsQuerySnapshot = await restaurantsCollection
            .where(
              categoryKey,
              isEqualTo: category,
            )
            .get();
      }

      LocationData currentLocationData =
          await LocationsService.getCurrentCoordinates();

      final userActionStatistics =
          await UserActionStadisticsOfflineProvider.getUserActionStadistics(
        context,
      );

      List<Map<String, Object>> restaurantsRecyclerObjectList =
          RestaurantsRecycler.buildRestaurantsRecyclerObjectList(
        restaurantsQuerySnapshot.docs,
        currentLocationData,
        userActionStatistics,
      );

      restaurantsRecyclerObjectList.sort((a, b) =>
          RestaurantsRecycler.compareRestaurantsRecyclerObjects(a, b));

      return RestaurantsRecycler(
        restaurantsRecyclerObjects: restaurantsRecyclerObjectList,
      );
    }

    return null;
  }

  static Future<Restaurant> getRestaurantByQrCode(
    BuildContext context,
    String qrCode,
  ) async {
    Restaurant restaurant;

    if (await checkConnectivity(context)) {
      final CollectionReference restaurantsCollection =
          FirebaseFirestore.instance.collection(restaurantsTableName);

      QuerySnapshot getRestaurantsQuerySnapshot;

      getRestaurantsQuerySnapshot = await restaurantsCollection
          .where(
            qrCodeKey,
            isEqualTo: qrCode,
          )
          .get();

      LocationData currentLocationData =
          await LocationsService.getCurrentCoordinates();

      final restaurantDoc = getRestaurantsQuerySnapshot.docs[0];
      restaurant = Restaurant.fromFirestore(
        restaurantDoc: restaurantDoc,
        currentLocationData: currentLocationData,
      );
    }

    return restaurant;
  }

  static List<Restaurant> filterRestaurants(
    BuildContext context,
    List<Restaurant> restaurants,
    String searchText,
  ) {
    checkConnectivity(context);
    return restaurants
        .where((restaurant) => restaurant
            .toString()
            .toLowerCase()
            .contains(searchText.toLowerCase()))
        .toList();
  }
}
