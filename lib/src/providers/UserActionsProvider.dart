import 'package:cloud_firestore/cloud_firestore.dart';
import 'package:firebase_auth/firebase_auth.dart';
import 'package:flutter/cupertino.dart';
import 'package:reservapp_flutter/src/model/Booking.dart';
import 'package:reservapp_flutter/src/model/Restaurant.dart';
import 'package:reservapp_flutter/src/model/UserAction.dart';
import 'package:reservapp_flutter/src/utils/checkConnectivity.dart';

final String userActionsTableName = 'userActions';

class UserActionsProvider {
  static Future<List<UserAction>> getUserActions(
    BuildContext context,
  ) async {
    final userId = FirebaseAuth.instance.currentUser.email;
    List<UserAction> userActions;

    if (await checkConnectivity(context)) {
      final CollectionReference userActionsCollection =
          FirebaseFirestore.instance.collection(userActionsTableName);

      final userActionsQuerySnapshot = await userActionsCollection
          .where(
            userIdKey,
            isEqualTo: userId,
          )
          .get();

      userActions = UserAction.makeUserActionListFromQuerySnapshot(
        userActionsQuerySnapshot,
      );
    }
    return userActions;
  }

  static Future<DocumentReference> postUserAction(
    BuildContext context,
    UserAction userAction,
  ) async {
    if (await checkConnectivity(context)) {
      final CollectionReference userActionsCollection =
          FirebaseFirestore.instance.collection(userActionsTableName);
      final userActionReference =
          await userActionsCollection.add(userAction.toFirebaseDoc());

      return userActionReference;
    } else {
      return null;
    }
  }

  static Future<DocumentReference> postUserActionFromRestaurant(
    BuildContext context,
    Restaurant restaurant,
    USER_ACTION_TYPES type,
    VIEWS view,
  ) async {
    final userAction = UserAction(
      restaurantId: restaurant.id,
      restaurantName: restaurant.name,
      restaurantAddress: restaurant.address,
      restaurantCategory: restaurant.category,
      type: type,
      view: view,
    );
    return await UserActionsProvider.postUserAction(
      context,
      userAction,
    );
  }

  static Future<DocumentReference> postUserActionFromBooking(
    BuildContext context,
    Booking booking,
    USER_ACTION_TYPES type,
    VIEWS view,
  ) async {
    final userAction = UserAction(
      restaurantId: booking.restaurantId,
      restaurantName: booking.restaurantName,
      restaurantAddress: booking.restaurantAddress,
      restaurantCategory: booking.restaurantCategory,
      type: type,
      view: view,
    );
    return await UserActionsProvider.postUserAction(
      context,
      userAction,
    );
  }
}
