import 'package:cloud_firestore/cloud_firestore.dart';
import 'package:firebase_auth/firebase_auth.dart';
import 'package:flutter/cupertino.dart';
import 'package:reservapp_flutter/src/model/Booking.dart';
import 'package:reservapp_flutter/src/model/UserAction.dart';
import 'package:reservapp_flutter/src/offlineProviders/BookingsOfflineProvider.dart';
import 'package:reservapp_flutter/src/utils/checkConnectivity.dart';

class BookingsProvider {
  static Future<List<Booking>> getBookings(
    BuildContext context,
  ) async {
    final userId = FirebaseAuth.instance.currentUser.email;
    List<Booking> bookings;

    if (await checkConnectivity(context)) {
      final CollectionReference bookingsCollection =
          FirebaseFirestore.instance.collection(
        bookingTableName,
      );

      final bookingsQuerySnapshot = await bookingsCollection
          .where(
            userIdKey,
            isEqualTo: userId,
          )
          .get();

      bookings = Booking.makeBookingListFromQuerySnapshot(
        bookingsQuerySnapshot,
      );
    } else {
      bookings = await BookingsOfflineProvider.getBookings(userId);
    }

    bookings.sort((a, b) => b.date.compareTo(a.date));

    return bookings;
  }

  static Future<DocumentReference> postBooking(
    BuildContext context,
    Booking booking,
  ) async {
    if (await checkConnectivity(context)) {
      final CollectionReference bookingsCollection =
          FirebaseFirestore.instance.collection(bookingTableName);
      final bookingReference = await bookingsCollection.add(
        booking.toFirebaseDoc(),
      );

      booking.id = bookingReference.id;
      await BookingsOfflineProvider.insert(booking);

      return bookingReference;
    }
    return null;
  }

  static Future<Booking> patchBooking(
    BuildContext context,
    Booking booking,
  ) async {
    if (await checkConnectivity(context)) {
      final CollectionReference bookingsCollection =
          FirebaseFirestore.instance.collection(bookingTableName);

      await bookingsCollection.doc(booking.id).update(booking.toFirebaseDoc());
      return booking;
    }
    return null;
  }

  static Future<void> deleteBookings(
    BuildContext context,
    String id,
  ) async {
    if (await checkConnectivity(context)) {
      final CollectionReference bookingsCollection =
          FirebaseFirestore.instance.collection(bookingTableName);

      return bookingsCollection.doc(id).delete();
    }
    return null;
  }
}
