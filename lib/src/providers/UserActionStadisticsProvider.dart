import 'package:firebase_auth/firebase_auth.dart';
import 'package:flutter/cupertino.dart';
import 'package:reservapp_flutter/src/model/UserAction.dart';
import 'package:reservapp_flutter/src/model/UserActionStatistics.dart';
import 'package:reservapp_flutter/src/offlineProviders/UserActionStadisticsOfflineProvider.dart';
import 'package:reservapp_flutter/src/providers/UserActionsProvider.dart';
import 'package:reservapp_flutter/src/utils/checkConnectivity.dart';

final String userActionStadisticsTableName = 'userActionStadistics';

class UserActionStadisticsProvider {
  static Future<UserActionStatistics> calculateUserActionStatistics(
    BuildContext context,
  ) async {
    final userId = FirebaseAuth.instance.currentUser.email;

    UserActionStatistics userActionStatistics = UserActionStatistics(
      userId: userId,
    );

    if (await checkConnectivity(context)) {
      final List<UserAction> userActions =
          await UserActionsProvider.getUserActions(
        context,
      );

      userActionStatistics.processUserActions(userActions);

      UserActionStadisticsOfflineProvider.setUserActionStadistics(
        userActionStatistics,
      );
    }

    return userActionStatistics;
  }
}
