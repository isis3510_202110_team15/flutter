import 'package:flutter/cupertino.dart';
import 'package:flutter/material.dart';
import 'package:reservapp_flutter/src/model/UserAction.dart';
import 'package:reservapp_flutter/src/model/UserActionStatistics.dart';
import 'package:reservapp_flutter/src/offlineProviders/SharedPreferencesProvider.dart';
import 'package:reservapp_flutter/src/pages/RestaurantsView.dart';
import 'package:reservapp_flutter/src/providers/UserActionsProvider.dart';
import 'package:reservapp_flutter/src/utils/checkConnectivity.dart';
import 'package:reservapp_flutter/src/widgets/containers/ViewContainer.dart';
import 'package:top_snackbar_flutter/custom_snack_bar.dart';
import 'package:top_snackbar_flutter/top_snack_bar.dart';

class NotificationsProvider {
  static void handleCategoryNotification(context) async {
    String favoriteRestaurantCategory =
        await SharedPreferencesProvider.getString(
      favoriteRestaurantCategoryKey,
    );

    void redirectToRestauransCategory() {
      final userAction = UserAction(
        restaurantCategory: favoriteRestaurantCategory,
        type: USER_ACTION_TYPES.viewCategory,
        view: VIEWS.notification,
      );
      UserActionsProvider.postUserAction(
        context,
        userAction,
      );

      Navigator.push(
        context,
        new MaterialPageRoute(
          builder: (BuildContext context) => ViewContainer(
            title: '$favoriteRestaurantCategory Restaurants',
            child: RestaurantsView(
              category: favoriteRestaurantCategory,
            ),
          ),
        ),
      );
    }

    bool hasConnectivity = await checkConnectivity(context);
    if (hasConnectivity && favoriteRestaurantCategory != null) {
      showTopSnackBar(
        context,
        CustomSnackBar.info(
          message:
              'The best $favoriteRestaurantCategory restaurants minutes away!',
        ),
        displayDuration: Duration(seconds: 3),
        onTap: redirectToRestauransCategory,
      );
    }
  }
}
