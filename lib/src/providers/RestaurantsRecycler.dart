import 'package:cloud_firestore/cloud_firestore.dart';
import 'package:location/location.dart';
import 'package:reservapp_flutter/src/model/Restaurant.dart';
import 'package:reservapp_flutter/src/model/UserActionStatistics.dart';
import 'package:reservapp_flutter/src/utils/LocationsService.dart';

final String restaurantDocKey = 'restaurantDoc';

class RestaurantsRecycler {
  final List<Map<String, Object>> restaurantsRecyclerObjects;
  List<Restaurant> restaurants = [];
  bool done = false;

  RestaurantsRecycler({
    this.restaurantsRecyclerObjects,
  });

  List<Restaurant> fetchNextRestaurants({int quantity: 4}) {
    Map<String, Object> restaurantRecyclerObject;
    int fetchedCount = 0;

    for (int i = restaurants.length;
        i < restaurantsRecyclerObjects.length && fetchedCount < quantity;
        fetchedCount++, i++) {
      restaurantRecyclerObject = restaurantsRecyclerObjects[i];
      restaurants.add(
        Restaurant.fromFirestore(
          restaurantDoc: restaurantRecyclerObject[restaurantDocKey],
          kmAway: restaurantRecyclerObject[kmAwayKey],
          userActionCount: restaurantRecyclerObject[userActionCountKey],
        ),
      );
    }

    done = restaurants.length == restaurantsRecyclerObjects.length;

    return restaurants;
  }

  static List<Map<String, Object>> buildRestaurantsRecyclerObjectList(
    List<QueryDocumentSnapshot> restaurantDocs,
    LocationData currentLocationData,
    UserActionStatistics userActionStatistics,
  ) {
    List<Map<String, Object>> restaurantsRecyclerObjectList = [];
    String currentRestauranId;
    int userActionCount;
    double latitude;
    double longitude;
    QueryDocumentSnapshot restaurantDoc;

    for (int i = 0; i < restaurantDocs.length; i++) {
      restaurantDoc = restaurantDocs[i];
      latitude = restaurantDoc[latitudeKey];
      longitude = restaurantDoc[longitudeKey];
      currentRestauranId = restaurantDoc.reference.id;
      userActionCount =
          userActionStatistics.restaurantIdStatistics[currentRestauranId];

      userActionCount = userActionCount == null ? 0 : userActionCount;

      double distanceInKm;
      if (currentLocationData != null) {
        distanceInKm = LocationsService.getKmBetweenTwoCoordinates(
          currentLocationData.latitude,
          currentLocationData.longitude,
          latitude,
          longitude,
        );
      }

      restaurantsRecyclerObjectList.add({
        restaurantDocKey: restaurantDoc,
        userActionCountKey: userActionCount,
        kmAwayKey: distanceInKm,
      });
    }

    return restaurantsRecyclerObjectList;
  }

  static int compareRestaurantsRecyclerObjects(
    Map<String, Object> a,
    Map<String, Object> b,
  ) {
    if (a[kmAwayKey] != null &&
        b[kmAwayKey] != null &&
        a[userActionCountKey] != null &&
        b[userActionCountKey] != null) {
      if (a[userActionCountKey] == b[userActionCountKey]) {
        return double.parse(a[kmAwayKey].toString())
            .compareTo(double.parse(b[kmAwayKey].toString()));
      }
      return double.parse(b[userActionCountKey].toString()).compareTo(
        double.parse(a[userActionCountKey].toString()),
      );
    }
    return 0;
  }
}
