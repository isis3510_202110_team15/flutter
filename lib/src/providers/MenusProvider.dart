import 'package:flutter/cupertino.dart';
import 'package:reservapp_flutter/src/model/Dish.dart';
import 'package:reservapp_flutter/src/model/Menu.dart';
import 'package:cloud_firestore/cloud_firestore.dart';
import 'package:reservapp_flutter/src/utils/checkConnectivity.dart';

class MenusProvider {
  static Future<Menu> getMenu(BuildContext context, String restaurantId) async {
    if (await checkConnectivity(context)) {
      final CollectionReference menusCollection =
          FirebaseFirestore.instance.collection(menuTableName);

      final getMenusQuerySnapshot = await menusCollection
          .where(
            restaurantIdKey,
            isEqualTo: restaurantId,
          )
          .get();

      final menu = getMenusQuerySnapshot.docs[0];

      List<Dish> drinks = Dish.makeDishesFromDocs(menu[drinksKey]);
      List<Dish> breakfasts = Dish.makeDishesFromDocs(menu[breakfastsKey]);
      List<Dish> entrees = Dish.makeDishesFromDocs(menu[entreesKey]);
      List<Dish> mains = Dish.makeDishesFromDocs(menu[mainsKey]);
      List<Dish> desserts = Dish.makeDishesFromDocs(menu[dessertsKey]);

      return Menu(
        drinks: drinks,
        breakfasts: breakfasts,
        entrees: entrees,
        mains: mains,
        desserts: desserts,
      );
    }

    return null;
  }
}
