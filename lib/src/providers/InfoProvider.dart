import 'package:flutter/material.dart';
import 'package:cloud_firestore/cloud_firestore.dart';
import 'package:firebase_auth/firebase_auth.dart';
import 'package:flutter/cupertino.dart';
import 'package:reservapp_flutter/src/model/Info.dart';
import 'package:reservapp_flutter/src/model/UserActionStatistics.dart';
import 'package:reservapp_flutter/src/providers/UserActionStadisticsProvider.dart';
import 'package:reservapp_flutter/src/utils/checkConnectivity.dart';

class InfoProvider {
  bool hasConnection;
  bool isNew = false;

  Future<Info> getQuantities(BuildContext context) async {
    final userId = FirebaseAuth.instance.currentUser.email;

    UserActionStatistics actions =
        await UserActionStadisticsProvider.calculateUserActionStatistics(
            context);

    Info example = new Info();

    List<Timestamp> a = [];
    String userCategory;
    String favCat;
    String favRest;
    String favRestId;
    String favHour = '';
    String favBarrio = "Mal";

    int morning = 0;
    int afternoon = 0;
    int evening = 0;

    int loneWolf = 0;
    int lover = 0;
    int socialite = 0;

    favCat = actions.favoriteRestaurantCategory;
    favRest = actions.favoriteRestaurantName;
    favRestId = actions.favoriteRestaurantId;

    List<int> quantities = [];

    if (await checkConnectivity(context)) {
      CollectionReference bookingsCollection =
          FirebaseFirestore.instance.collection('bookings');
      CollectionReference restCollection =
          FirebaseFirestore.instance.collection('restaurants');

      final getBookingsQuerySnapshot =
          await bookingsCollection.where('userId', isEqualTo: userId).get();
      final getRestQuerySnapshot = await restCollection.doc(favRestId).get();

      if (getBookingsQuerySnapshot.docs.length == 0) {
        isNew = true;
      } else {
        isNew = false;
      }

      for (var i = 0; i < getBookingsQuerySnapshot.docs.length; i++) {
        quantities.add(getBookingsQuerySnapshot.docs[i]['peopleQuantity']);
        a.add(getBookingsQuerySnapshot.docs[i]['date']);
      }

      Map<String, dynamic> data = getRestQuerySnapshot.data();

      favBarrio = data['neighborhood'];

      for (var i = 0; i < a.length; i++) {
        if (a[i].toDate().hour >= 6 && a[i].toDate().hour <= 11) {
          morning++;
        }
        if (a[i].toDate().hour > 11 && a[i].toDate().hour <= 17) {
          afternoon++;
        }
        if (a[i].toDate().hour > 17 && a[i].toDate().hour <= 23) {
          afternoon++;
        }
        if (a[i].toDate().hour >= 0 && a[i].toDate().hour < 6) {
          afternoon++;
        }
      }

      if (morning > afternoon && morning > evening) {
        favHour = "Morning";
      }
      if (afternoon > morning && afternoon > evening) {
        favHour = "Afternoon";
      }
      if (evening > afternoon && evening > morning) {
        favHour = "Evening";
      }

      for (var i = 0; i < quantities.length; i++) {
        if (quantities[i] == 1) {
          loneWolf++;
        }
        if (quantities[i] == 2) {
          lover++;
        }
        if (quantities[i] > 2) {
          socialite++;
        }
      }

      if (loneWolf > lover && loneWolf > socialite) {
        userCategory = "LoneWolf";
      }
      if (lover > loneWolf && lover > socialite) {
        userCategory = "Lover";
      }
      if (socialite > loneWolf && socialite > lover) {
        userCategory = "Socialite";
      }

      hasConnection = true;

      example.favCat = favCat;
      example.favRest = favRest;
      example.favTime = favHour;
      example.userCategory = userCategory;
      example.favNeigh = favBarrio;

      return example;
    } else {
      hasConnection = false;
      return example;
    }
  }
}
