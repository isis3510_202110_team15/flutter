import 'package:cloud_firestore/cloud_firestore.dart';
import 'package:firebase_auth/firebase_auth.dart';
import 'package:flutter/cupertino.dart';
import 'package:reservapp_flutter/src/model/User.dart';
import 'package:reservapp_flutter/src/offlineProviders/DatabaseProvider.dart';
import 'package:reservapp_flutter/src/offlineProviders/SharedPreferencesProvider.dart';
import 'package:reservapp_flutter/src/utils/checkConnectivity.dart';
import '../model/User.dart';

final String allergiesTableName = 'allergies';

final String nameKey = 'name';

class AllergiesProvider {
  static Future<Map<String, bool>> getAllergies(
    BuildContext context,
  ) async {
    final userId = FirebaseAuth.instance.currentUser.email;
    Map<String, bool> allergies = {};

    if (await checkConnectivity(context)) {
      CollectionReference allergiesCollection =
          FirebaseFirestore.instance.collection(allergiesTableName);
      final getAllergiesQuerySnapshot = await allergiesCollection.get();

      List<String> allergyNames = [];
      for (var i = 0; i < getAllergiesQuerySnapshot.docs.length; i++) {
        allergyNames.add(getAllergiesQuerySnapshot.docs[i][nameKey]);
      }

      allergyNames.sort((a, b) => a.compareTo(b));

      for (var i = 0; i < allergyNames.length; i++) {
        allergies[allergyNames[i]] = false;
      }

      final CollectionReference usersCollection =
          FirebaseFirestore.instance.collection(usersTableName);
      final completeUser = await usersCollection.doc(userId).get();

      completeUser[allergiesKey].forEach(
        (allergyDoc) => allergies[allergyDoc[nameKey]] = true,
      );
    } else {
      final allergiesNames = await SharedPreferencesProvider.getStringList(
        allergiesTableName,
      );

      for (var i = 0; i < allergiesNames.length; i++) {
        allergies[allergiesNames[i]] = true;
      }
    }

    return allergies;
  }

  static Future<bool> patchAllergy(
    BuildContext context,
    String allergyName,
    bool allergyStatus,
  ) async {
    if (await checkConnectivity(context)) {
      final userId = FirebaseAuth.instance.currentUser.email;
      final CollectionReference usersCollection =
          FirebaseFirestore.instance.collection(usersTableName);
      final completeUser = await usersCollection.doc(userId).get();
      List<dynamic> allergies = completeUser[allergiesKey];

      List<dynamic> newAllergies = [];

      if (allergyStatus) {
        newAllergies = allergies;
        final completeAllergy = await getAllergy(context, allergyName);
        newAllergies.add(completeAllergy);
      } else {
        for (var i = 0; i < allergies.length; i++) {
          if (allergies[i][nameKey] != allergyName) {
            newAllergies.add(allergies[i]);
          }
        }
      }

      List<String> allergiesNames = newAllergies
          .map(
            (allergy) => allergy[nameKey].toString(),
          )
          .toList();
      await SharedPreferencesProvider.setStringList(
        allergiesTableName,
        allergiesNames,
      );

      await usersCollection.doc(userId).update(
        {
          allergiesKey: newAllergies,
        },
      );

      return allergyStatus;
    }

    return !allergyStatus;
  }

  static Future<Map<String, Object>> getAllergy(
    BuildContext context,
    String allergyName,
  ) async {
    if (await checkConnectivity(context)) {
      CollectionReference allergiesCollection =
          FirebaseFirestore.instance.collection(allergiesTableName);
      final getAllergiesQuerySnapshot = await allergiesCollection
          .where(
            nameKey,
            isEqualTo: allergyName,
          )
          .get();
      final allergyDoc = getAllergiesQuerySnapshot.docs[0];
      final allergy = {
        idKey: allergyDoc.reference.id,
        nameKey: allergyDoc[nameKey],
      };

      return allergy;
    }

    return null;
  }
}
