import 'package:flutter/cupertino.dart';
import 'package:flutter/material.dart';
import 'package:reservapp_flutter/src/widgets/containers/CardContainer.dart';

void showGeneralCardDialog({BuildContext context, Widget child}) async {
  showGeneralDialog(
    context: context,
    barrierDismissible: true,
    barrierLabel: MaterialLocalizations.of(context).modalBarrierDismissLabel,
    transitionDuration: Duration(milliseconds: 500),
    pageBuilder: (BuildContext context, Animation first, Animation second) {
      return Container(
        child: CardContainer(
          child: child,
        ),
      );
    },
  );
}
