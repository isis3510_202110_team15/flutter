import 'dart:math';
import 'package:location/location.dart';
import 'package:reservapp_flutter/src/offlineProviders/SharedPreferencesProvider.dart';

final double degreesInAKm = 111.139;
final String locationDenniedKey = 'locationDennied';

class LocationsService {
  static double getKmBetweenTwoCoordinates(
    double latitude1,
    double longitude1,
    double latitude2,
    double longitude2,
  ) {
    double longitudeDistance = longitude2 - longitude1;
    double latitudeDistance = latitude2 - latitude1;

    // pythagorean theorem
    double x2 = longitudeDistance * longitudeDistance;
    double y2 = latitudeDistance * latitudeDistance;

    double distance = sqrt(x2 + y2);
    double distanceInKm = distance * degreesInAKm;

    return distanceInKm;
  }

  static Future<LocationData> getCurrentCoordinates() async {
    bool locationDennied = await SharedPreferencesProvider.getBool(
      locationDenniedKey,
    );

    if (locationDennied != true) {
      final location = Location();

      bool serviceEnabled = await location.serviceEnabled();
      if (!serviceEnabled) {
        serviceEnabled = await location.requestService();
        if (!serviceEnabled) {
          SharedPreferencesProvider.setBool(locationDenniedKey, true);
          return null;
        }
      }

      PermissionStatus permissionGranted = await location.hasPermission();
      if (permissionGranted == PermissionStatus.denied) {
        permissionGranted = await location.requestPermission();
        if (permissionGranted != PermissionStatus.granted) {
          SharedPreferencesProvider.setBool(locationDenniedKey, true);
          return null;
        }
      }

      return location.getLocation();
    }
    return null;
  }

  static Future<double> getCurrentDistanceToCoordinatesInKm(
    double latitude,
    double longitude,
  ) async {
    LocationData currentLocationData =
        await LocationsService.getCurrentCoordinates();

    double distanceInKm = LocationsService.getKmBetweenTwoCoordinates(
      currentLocationData.latitude,
      currentLocationData.longitude,
      latitude,
      longitude,
    );

    return distanceInKm;
  }
}
