dynamic stringToEnum(
  dynamic enumObject,
  String stringValue,
) {
  dynamic enumValue;

  for (var i = 0; i < enumObject.values.length; i++) {
    if (enumObject.values[i].toString() == stringValue) {
      enumValue = enumObject.values[i];
    }
  }
  return enumValue;
}
