import 'package:flutter/cupertino.dart';
import 'package:reservapp_flutter/src/pages/AnalyticsView.dart';
import 'package:reservapp_flutter/src/pages/BookingsView.dart';
import 'package:reservapp_flutter/src/pages/RestaurantsView.dart';
import 'package:reservapp_flutter/src/pages/HomeView.dart';
import 'package:reservapp_flutter/src/pages/LoginView.dart';
import 'package:reservapp_flutter/src/pages/SignupView.dart';
import 'package:reservapp_flutter/src/widgets/containers/ViewContainer.dart';

enum ROUTES {
  login,
  signup,
  home,
  restaurants,
  bookings,
  analytics,
}

final Map<String, Widget Function(BuildContext)> routes = {
  ROUTES.login.toString(): (BuildContext context) => LoginView(),
  ROUTES.signup.toString(): (BuildContext context) => SignupView(),
  ROUTES.home.toString(): (BuildContext context) => ViewContainer(
        title: 'Categories',
        child: HomeView(),
      ),
  ROUTES.restaurants.toString(): (BuildContext context) => ViewContainer(
        title: 'Restaurants',
        child: RestaurantsView(),
      ),
  ROUTES.bookings.toString(): (BuildContext context) => ViewContainer(
        title: 'Bookings',
        child: BookingsView(),
      ),
  ROUTES.analytics.toString(): (BuildContext context) => ViewContainer(
        title: 'You have great taste!',
        child: AnalyticsView(),
      ),
};
